# Container

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [What is Container](#what-is-container)
    1. [Function of Container](#function-of-container)
    2. [Characters of Container](#characters-of-container)
2. [History of Container](#history-of-container)
3. [Container in Deep](#container-in-deep)
4. [VM vs Container](#vm-vs-container)
5. [Docker and Kubernetes](#docker-and-kubernetes)
6. [Docker with VM](#docker-with-vm)
7. [Futhermore](#futhermore)

<!-- /code_chunk_output -->

## Why Uses Container

环境配置problem，具体表现在：

1. 环境一致性：开发机器与运行机器的环境不一致
1. 环境混乱：开发机器因为多个项目不同的环境配置需求导致环境混乱
1. 环境配置劳动密集型：手动配置环境需要大量劳动，`shell`脚本配置开发难度较大且可能存在目标机器环境导致`shell`脚本配置不成功

## What is Container

### Function of Container

1. application packaging && publishing tool
1. application deploy && runtime tool

### Characters of Container

1. light-weight resource isolation and control
1. `OS` level resource isolation and control

## History of Container

1. 1979: `UNIX V7 chroot`
1. 2000: `FreeBSD Jails`
1. 2001-10: [`Linux VServer`](https://linux-vserver.org)
1. 2004: `Solaris Containers`
1. 2005: [`Linux Open VZ(Open VirtuZzo)`](https://openvz.org)
1. 2006: `Linux Process Containers(Linux Control Groups/cgroups)`, by `Google`
1. 2008: `Linux Containers/LXC`, combined with `Linux cgroups` and `Linux namespaces`
1. 2011: `CloudFoundry Warden`
1. 2013: `Google LMCTFY`, stopped in 2015
1. 2013: `Docker`
1. 2014: `CoreOS Rocket`
1. 2016: `Windows Containers`, support `Docker` with `OS kernel`

>1. [40 年回顾，一文读懂容器发展史](https://www.infoq.cn/article/SS6SItkLGoLExQP4uMr5)
>1. [容器简史：从1979至今的日子](https://www.freebuf.com/articles/network/229004.html)

## Container in Deep

```math
container = cgroups + namespaces + roofs + runtime
```

1. `cgroups`：资源控制，用于限制和隔离一组进程对系统资源的使用
1. `namespaces`：访问隔离，将内核的全局资源进行封装，使得每个namespace都有一份独立的资源，不同的进程在各自的namespace中对同一种资源的使用互不干扰
1. `roofs`：文件系统隔离

## VM vs Container

![virtual-machines_versus_containers](./.assets_image/virtual-machines_versus_containers.png)

>[Virtual Machine VS Container](https://medium.com/@deshanigeethika/docker-tutorial-a6aa5b41e3ff)

||VM|Container|
|--|--|--|
|isolation|physical resource|application|
|abstract|machine|process and sub-process|
|sharing|host hardware|host(Linux) hardware, kernel, libraries|

1. Containers virtualize the Operating System and make them more portable
1. Virtual Machines, by contrast, virtualize the hardware.
1. Containers are an abstraction at the application layer that packages code and dependencies together.
1. VMs are an abstraction of physical hardware turning one server into many.

容器没有自己的内核，也没有进行硬件虚拟化，因此，`container`比`VM`要轻便

每个容器有自己的文件系统，因此，`container`之间能进行必要的隔离

## Docker and Kubernetes

像`TCP/IP`定义了`Internet`协议栈、`Unix`定义了`modern OS`一样，`Docker`事实上定义了`application packaging & publishing`、`Kubernetes`事实上定义了`application operating life cycle`

## Docker with VM

![docker-containers-are-not-lightweight-virtual-machines](./.assets_image/docker-containers-are-not-lightweight-virtual-machines.png)

>[Kubernetes vs Docker - What Is the Difference?](https://www.nakivo.com/blog/docker-vs-kubernetes/)

![container-based-cloud-vs-VM-based-cloud](./.assets_image/container-based-cloud-vs-VM-based-cloud.png)

>[A hybrid genetic programming hyper-heuristic approach for online two-level resource allocation in container-based clouds](https://meiyi1986.github.io/publication/tan-2019-hybrid/tan-2019-hybrid.pdf)

## Futhermore

1. What is a Container?-A standardized unit of software: <https://www.docker.com/resources/what-container>
1. Containers are not VMs: <https://www.docker.com/blog/containers-are-not-vms/>
1. Are Containers Replacing Virtual Machines?: <https://www.docker.com/blog/containers-replacing-virtual-machines/>
1. 40 年回顾，一文读懂容器发展史： <https://www.infoq.cn/article/SS6SItkLGoLExQP4uMr5>
1. 关于容器、微服务、docker 的十大问题： <https://www.infoq.cn/article/iqrVKSrBzZzo64h3umAW>
