# Docker Topic

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Timezone](#timezone)
    1. [MacOS Host](#macos-host)
    2. [Ubuntu](#ubuntu)
    3. [CentOS](#centos)
    4. [Alpine](#alpine)

<!-- /code_chunk_output -->

## Timezone

For general purpose, most `image` use `UTC` as their default timezone. In this way, when running `container`, should change the timezone to correct.

Solution: install `tzdata` in `container`, then change timezone through `tzdata`

### MacOS Host

```bash {.line-numbers}
# get timezone in MacOS
export TZ=$(readlink /etc/localtime | sed 's#/var/db/timezone/zoneinfo/##');

# then docker run
docker run -e "TZ=${TZ}"
```

### Ubuntu

```bash {.line-numbers}
# when building image, in Dockerfile

apt-get update && apt install tzdata

# when running container
docker run -v /etc/timezone:/etc/timezone:ro [ubuntu-image-name]
docker run -v /etc/localtime:/etc/localtime:ro [ubuntu-image-name]
```

### CentOS

```bash {.line-numbers}
docker run -e TZ=${your_timezone} [centos-image-name]
```

### Alpine

```bash {.line-numbers}
FROM alpine
RUN apk add --no-cache tzdata \
    && ln -snf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \
    && echo "Asia/Shanghai" > /etc/timezone
ENV TZ Asia/Shanghai
```

## Docker Desktop