# Docker Command

## Management Commands and Traditional Solo Commands

Docker 1.13.0(2017-01-18) introduced grouped commands to help organize a bunch of Docker commands. Both commands do the same thing.

>1. [_Introducing Docker 1.13_. 2017-01-19.](https://www.docker.com/blog/whats-new-in-docker-1-13/)
>1. [_Docker Management Commands_.](https://rominirani.com/docker-management-commands-e89a23c55908)
>1. [_Docker 1.13 Management Commands_](https://blog.couchbase.com/docker-1-13-management-commands/)

1.12|1.13|Purpose
--|--|--
attach|container attach|Attach to a running container
build|image build|Build an image from a Dockerfile
commit|container commit|Create a new image from a container’s changes
cp|container cp|Copy files/folders between a container and the local filesystem
create|container create|Create a new container
diff|container diff|Inspect changes on a container’s filesystem
events|system events|Get real time events from the server
exec|container exec|Run a command in a running container
export|container export|Export a container’s filesystem as a tar archive
history|image history|Show the history of an image
images|image ls|List images
import|image import|Import the contents from a tarball to create a filesystem image
info|system info|Display system-wide information
inspect|container inspect|Return low-level information on a container, image or task
kill|container kill|Kill one or more running containers
load|image load|Load an image from a tar archive or STDIN
login|login|Log in to a Docker registry.
logout|logout|Log out from a Docker registry.
logs|container logs|Fetch the logs of a container
network|network|Manage Docker networks
node|node|Manage Docker Swarm nodes
pause|container pause|Pause all processes within one or more containers
port|container port|List port mappings or a specific mapping for the container
ps|container ls|List containers
pull|image pull|Pull an image or a repository from a registry
push|image push|Push an image or a repository to a registry
rename|container rename|Rename a container
restart|container restart|Restart a container
rm|container rm|Remove one or more containers
rmi|image rm|Remove one or more images
run|container run|Run a command in a new container
save|image save|Save one or more images to a tar archive (streamed to STDOUT by default)
search|search|Search the Docker Hub for images
service|service|Manage Docker services
start|container start|Start one or more stopped containers
stats|container stats|Display a live stream of container(s) resource usage statistics
stop|container stop|Stop one or more running containers
swarm|swarm|Manage Docker Swarm
tag|image tag|Tag an image into a repository
top|container top|Display the running processes of a container
unpause|container unpause|Unpause all processes within one or more containers
update|container update|Update configuration of one or more containers
version|version|Show the Docker version information
volume|volume|Manage Docker volumes
wait|container wait|Block until a container stops, then print its exit code
