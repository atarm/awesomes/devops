# 飞书Lark-FeiShu

## `飞书`与`禅道`集成

1. [集成飞书消息通知](https://www.zentao.net/book/zentaopmshelp/477.mhtml)

## `飞书`与`Lark`

1. [`lark`和`飞书`的区别.](https://www.36dianping.com/qa/200.html)

## 价格

1. [`飞书`版本及服务](https://www.feishu.cn/service)
1. [`lark`版本及服务](https://www.larksuite.com/plans)
