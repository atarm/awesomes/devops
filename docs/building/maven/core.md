# Maven原理与核心Maven Principles and Cores

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Maven核心概述Outline of Maven Cores](#maven核心概述outline-of-maven-cores)
2. [pom.xml](#pomxml)
3. [Coordinates坐标](#coordinates坐标)
4. [Repositories仓库](#repositories仓库)
    1. [Mirrors镜像仓库](#mirrors镜像仓库)
    2. [Proxies代理仓库](#proxies代理仓库)
    3. [Resources](#resources)
        1. [工件搜索Artifact Search](#工件搜索artifact-search)
        2. [Documentations](#documentations)
5. [Dependencies依赖](#dependencies依赖)
    1. [Scope依赖范围](#scope依赖范围)
    2. [依赖传递](#依赖传递)
    3. [依赖解析](#依赖解析)
    4. [依赖导入](#依赖导入)
6. [生命周期与插件](#生命周期与插件)
    1. [生命周期模型与阶段Lifecycle and `phase`](#生命周期模型与阶段lifecycle-and-phase)
    2. [插件与目标`plugin` and `goal`](#插件与目标plugin-and-goal)
    3. [插件的配置与绑定](#插件的配置与绑定)
    4. [常用插件](#常用插件)
        1. [官方插件](#官方插件)
        2. [第三方插件](#第三方插件)
7. [Maven项目的标准目录结构](#maven项目的标准目录结构)
8. [常用命令](#常用命令)
9. [Futhermore](#futhermore)

<!-- /code_chunk_output -->

<!--TODO:
1. POM体系
    1. 属性
1. 坐标体系
1. 生命周期与插件体系
1. 依赖与仓库体系
-->

## Maven核心概述Outline of Maven Cores

1. `pom.xml`：`POM(Project Object Model)`

1. 约定的目录结构
1. `POM`
1. `maven`坐标：
    1. `groupId`：公司或组织名称的倒序 + 项目名称
    1. `artifactId`：模块名称
    1. `version`：
    1. `packaging`, `classfilier`.
1. `dependency`依赖：
    1. `scope`依赖范围：
1. `repository`仓库
1. `lifecycle`（构建）生命周期：
1. `plugin and goal`插件与目标：
1. `inheritance`继承
1. `aggregation`聚合

## pom.xml

`super pom`：`Maven3.0`以上版本，为`${M2_HOME}/lib/maven-model-builder-x.y.z.jar`下的`org/apache/maven/model/pom-4.0.0.xml`

`super pom`是所有`Maven project`的`parent pom`

1. [Apache. POM Reference.](https://maven.apache.org/pom.html)
1. [Apache. maven-4.0.0.xsd](https://maven.apache.org/xsd/maven-4.0.0.xsd)

`effective pom`

## Coordinates坐标

1. `groupId`(required, no default): 定义当前`Maven project`隶属的实际项目，对应实际项目（公司或组织+实际项目名称），通常与`package`的命名方法一致（即，域名的反向）
1. `artifactId`(required, no default): 对应定义实际项目的模块（即，`Maven`项目），为方便寻找工件推荐将实际项目名称作为`artifactId`的前缀
1. `version`(required, no default): 定义
1. `packaging`(optional, default is `jar`):
1. `classifier`(undefinable): 不能在`pom.xml`中直接定义`classifier`，因为附属工件是由`plugin`生成的，而不是由`Maven project`直接生成的

项目工件的文件名与坐标相对应，一般的规则为`artifactId-version[-classifier].packaging`（工件文件扩展名并非一定与`packaging`相同，例如，`packaging`为`maven-plugin`的工件文件扩展名为`jar`）

待计算版本：

- 计算依据为远程仓库中的`groupId/artifactId/maven-metadata.xml`
    - `RELEASE`：最新的发布版本
    - `LATEST`：最新的版本（可能是`SNAPSHOT`）
- 计算依据为远程仓库中的`groupId/artifactId/version/maven-metadata.xml`
    - `SNAPSHOT`：带时间戳的易变版本

## Repositories仓库

按仓库存储的工件类型分类：

1. 依赖仓库
1. 插件仓库

按仓库是否提供公共服务分类：

1. 公共仓库：在公网提供免费服务的仓库
1. 私服仓库：内部使用，不在公网提供服务的仓库
1. 商用仓库：在公网提供收费服务的仓库（实际上是否有这样的仓库 ❓）

按仓库是否在本地机器分类：

1. 本地仓库
1. 远程仓库

按远程仓库的作用分类：

1. 镜像仓库
1. 代理仓库
1. 非镜像非代理仓库

`central`指的是由`Maven`官方提供的一个包括`依赖仓库`和`插件仓库`的公共远程仓库

`super pom`中关于`central`的配置：

```xml {.line-numbers}
<repositories>
    <repository>
      <id>central</id>
      <name>Central Repository</name>
      <url>https://repo.maven.apache.org/maven2</url>
      <layout>default</layout>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
    </repository>
  </repositories>

  <pluginRepositories>
    <pluginRepository>
      <id>central</id>
      <name>Central Repository</name>
      <url>https://repo.maven.apache.org/maven2</url>
      <layout>default</layout>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
      <releases>
        <updatePolicy>never</updatePolicy>
      </releases>
    </pluginRepository>
  </pluginRepositories>
```

### Mirrors镜像仓库

### Proxies代理仓库

### Resources

#### 工件搜索Artifact Search

1. 🌟 [MVN Repository Search](https://mvnrepository.com/)
1. [Sonatype](https://repository.sonatype.org/)
1. 🌟 Maven Central Repository
    1. [Maven Central Repository Search](https://search.maven.org)
    1. [Maven Central Repository](https://repo.maven.apache.org/maven2)
1. 🌟 AliYun Maven Repository
    1. [阿里云云效Maven指南](https://maven.aliyun.com/mvn/guide)
    1. [阿里云云效Maven搜索](https://maven.aliyun.com/mvn/search)
1. [Google's Maven Repository](https://maven.google.com/web/index.html)

#### Documentations

1. [Sonatype. Maven Repositories](https://help.sonatype.com/repomanager3/nexus-repository-administration/formats/maven-repositories)
1. [Sonatype. The Central Repository Documentation](https://central.sonatype.org/)

## Dependencies依赖

>1. [apache.org. Introduction to the Dependency Mechanism.](https://maven.apache.org/guides/introduction/introduction-to-dependency-mechanism.html)

### Scope依赖范围

|scope|compile classpath|test classpath|runtime classpath|example|
|:--|:--:|:--:|:--:|:--|
|compile|Y|Y|Y|大部分类库|
|test|--|Y|--|测试类库|
|provided|Y|Y|--|`servlet-api`|
|runtime|--|Y|Y|`JDBC`的实现类库|
|system|Y|Y|--|本地类库文件（`Maven`仓库之外）|

### 依赖传递

### 依赖解析

### 依赖导入

以`-SNAPSHOT`结尾的版本号会被Maven视为开发版本，开发版本每次都会重复下载

<!--
2.2：
依赖范围的传递
当第二直接依赖范围是compile，传递行依赖范围和第一直接依赖的范围一致
当第二直接依赖范围是provided，只传递provided
当。。。是test，依赖不传递
当。。。是runtime，传递性除compile变成runtime外，其他都与第一直接依赖一致。

2.3
依赖调节：
依赖重复时首先路径最近者优先，其次第一申明者优先。

-->

<!--

（4）三个生命周期和插件执行
4.1
clean 清理项目
default 构建项目
site 建立项目站点

4.2
maven支持通过mvn命令激活生命周期阶段（通过phase的值绑定），从而执行那些绑定在生命阶段的插件目标
还支持直接从命令行调用插件目标。
4.3
生命周期内置插件绑定和具体任务
default生命周期举例：
生命周期阶段 插件目标 执行任务
process-resources maven-resources-plugin：resources 复制主要资源到主输出目录
compile maven-compiler-plugin：compile 编译主代码到主输出目录
test maven-surefire-plugin：test 执行测试用例
package maven-jar-plugin：jar 创建项目jar包
install maven-install-plugin：install 将项目输出构建安装到本地仓库
deploy maven-deploy-plugin：deploy 将项目输出构建部署到远程仓库

4.4
显示依赖关系的命令：
mvn dependency：list：显示全部依赖
mvn dependency：tree：显示树形依赖关系
mvn dependency：analyze：会提示used undeclared dependencies和unused delared dependencies

（5）常见插件：
maven-compiler-plugin：编译
maven-surefire-plugin：测试
maven-release-plugin：版本发布
maven-site-plugin：站点发布

（6）maven提供聚合和继承的用法
（7）nexus构建宿主或者代理仓库，hudson实现自动构建等。
（8）约定大于配置

-->

## 生命周期与插件

生命周期定义了构建的流程步骤`phase`，`phase`中具体的任务行为`task`由插件定义（具体由插件的`goal`定义），每个`phase`可以绑定一个或多个`goal`（按绑定的先后顺序依次执行）。

1. [Apache. Plugins Supported By The Maven Project.](https://maven.apache.org/plugins/index.html#supported-by-the-maven-project)
    1. [org/apache/maven/plugins](https://repo.maven.apache.org/maven2/org/apache/maven/plugins/)
1. [Apache. Plugins At MojoHaus (formerly known as codehaus.org)](https://maven.apache.org/plugins/index.html#at-mojohaus-formerly-known-as-codehaus-org)
    1. [MojoHaus. Plugins.](https://www.mojohaus.org/plugins.html)

### 生命周期模型与阶段Lifecycle and `phase`

>1. [apache.org. Introduction to the Build Lifecycle.](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html)
>    1. [apache.org. Lifecycle Reference.](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html#lifecycle-reference)

三套独立的生命周期模型

1. `clean`: 目的是清理项目的构建产出物
    1. `clean` - remove all files generated by the previous build
1. `default`: 目的是构建项目的产出物
    1. `validate` - validate the project is correct and all necessary information is available
    1. `initialize` - initialize build state, e.g. set properties or create directories.
    1. `compile` - compile the source code of the project
    1. `test` - test the compiled source code using a suitable unit testing framework. These tests should not require the code be packaged or deployed
    1. `package` - take the compiled code and package it in its distributable format, such as a JAR.
    1. `verify` - run any checks on results of integration tests to ensure quality criteria are met
    1. `install` - install the package into the local repository, for use as a dependency in other projects locally
    1. `deploy` - done in the build environment, copies the final package to the remote repository for sharing with other developers and projects.
1. `site`: 目的是生成项目信息站点
    1. `site` - generate the project's site documentation
    1. `site-deploy` - deploy the generated site documentation to the specified web server

### 插件与目标`plugin` and `goal`

### 插件的配置与绑定

>In Maven, there are two kinds of plugins, build and reporting:
>Build plugins are executed during the build and configured in the <build/> element.
>Reporting plugins are executed during the site generation and configured in the <reporting/> element.
>
>>[Apache. Guide to Configuring Plug-ins.](https://maven.apache.org/guides/mini/guide-configuring-plugins.html)

>1. [apache.org. Built-in Lifecycle Bindings.](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html#built-in-lifecycle-bindings)
>1. [apache.org. Guide to Configuring Plug-ins](https://maven.apache.org/guides/mini/guide-configuring-plugins.html)

### 常用插件

#### 官方插件

#### 第三方插件

## Maven项目的标准目录结构

1. [Apache. Introduction to the Standard Directory Layout.](https://maven.apache.org/guides/introduction/introduction-to-the-standard-directory-layout.html)

```sh {.line-numbers}
${basedir}
|-- pom.xml
|-- src
|    |-- main
|    |    `-- java
|    |    `-- resources
|    |    `-- filters
|    `-- test
|    |    `-- java
|    |    `-- resources
|    |    `-- filters
|    `-- it
|    `-- assembly
|    `-- site
|-- target
`-- LICENSE
`-- NOTICE
`-- README
```

<https://jerrychin.github.io/2020/04/27/alibaba-pmd-checking.html>

## 常用命令

```bash {.line-numbers}
# plugin docs
mvn help:describe -Dplugin=<plugin_coordinate>

# project dependencies information
mvn dependency:list
mvn dependency:tree
mvn dependency:analyze
```

## Futhermore
