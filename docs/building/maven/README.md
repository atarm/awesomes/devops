# Maven

## What - Maven是什么，能做什么

`Maven` means "Accumulator of Knowledge".

`Maven`中文翻译可以是"专家"或"行家"（口语）等，是`Apache`组织的一个顶级项目，声明式的、用于`Java`项目的信息管理、依赖管理和构建管理。

### Maven的主要用途

以功能模块划分：

1. 项目信息管理documentation
    1. 项目默认约定：项目目录结构、测试用例命名
    1. 项目静态信息：项目描述、开发者列表、版本控制系统地址、软件许可证、缺陷管理系统地址等
    1. 项目动态报告：测试报告、版本日志报告、项目站点等
1. 类库依赖管理dependencies
    1. 工件依赖
    1. 插件依赖
1. 源码构建管理builds：`Maven`抽象并设计了一套完整的构建生命周期模型，这套模型也标准化了构建流程
    1. 构建流程
    1. 工件分发

### Maven的核心理念/设计哲学

1. 抽象统一：如，生命周期将所有的构建过程进行抽象和统一
    1. 构建流程复用：`Maven`定义了一个标准的、完整的构建生命周期模型，使用插件`plugin`执行实际的构建（而不是自定义构建脚本）
1. 约定优于配置：`Maven`使用`pom.xml`定义项目信息（项目配置），并使用预设的标准目录结构
1. `Maven`中声明一个依赖项可以自动下载并导入`classpath`
1. 坐标定位构件：`Maven`使用`groupId`，`artifactId`和`version`唯一定位一个构件
1. `Maven`使用插件

## Why - 为什么需要Maven

the problem:

1. `IDE`的固有缺陷
    1. 依赖手工操作：编译、测试、打包等工作相互独立，很难一键完成所有工作，依赖大量的手工操作
    1. 个性化配置：很难在项目中统一团队所有人的`IDE`配置，容易导致"A机器成功、B机器失败"的现象
1. `Ant`的固有缺陷
    1. 手动管理依赖：没有内置的依赖管理功能（可以借助`Ivy`管理依赖）
    1. 构建脚本复杂：基于命令式的构建流程定义，需自定义构建规则和流程

Maven的优势：

1. easy build process
1. central repositories for dependencies
1. universal build system
1. make new features easy❓

## When & Where & Who - 谁在什么时候什么场景发明了Maven

## When & Where & Who - 谁应该在什么时候什么场景使用Maven

## Effect - 使用Maven的效果

### Negative Effects and Solutions

1. `IDE`支持度
    - situation：`IDE`的`Maven plugin`可能导致`Maven`版本及配置不一致、不稳定等情况
    - solution：尽可能通过`cli`使用`Maven`，将`IDE`的`Maven plugin`作为辅助工具（如：查看`Maven`配置）
1. 依赖之间的兼容性
    - situation：工件之间、插件之间、工件与插件之间可能出现版本兼容性问题
    - solution：本质是不是依赖管理工具需要解决的问题，而是项目本身要解决的问题
1. `Maven`配置复杂度高

### Positive Effects

## How - 如何学习Maven

## Terms

1. `mojo(Maven plain Old Java Object/Maven Ordinary Java Object)`: 一个`mojo`对应`plugin`中一个`goal`

## Futhermore

### Plugins

1. [Apache Maven Plugins](https://maven.apache.org/plugins/)
1. [MojoHaus Maven Plugins](https://www.mojohaus.org/plugins.html)

### Tools

1. [takari/maven-wrapper](https://github.com/takari/maven-wrapper)
    1. [maven-wrapper archived](https://github.com/takari/maven-wrapper)
    1. [apache/maven-wrapper](https://github.com/apache/maven-wrapper)

### Manuals and CheatSheets

1. 👑 [Official Maven Documentation](https://maven.apache.org/guides/index.html)
    1. [Maven Plugins](https://maven.apache.org/plugins/index.html)
    1. [Maven Extensions](https://maven.apache.org/extensions/index.html)
    1. [Lifecycles Reference](https://maven.apache.org/ref/current/maven-core/lifecycles.html)
1. 👑 [apache.org. Available Plugins(official).](https://maven.apache.org/plugins/index.html)
1. 👑 [apache.org. Maven CLI Options Reference(`mvn -h`).](https://maven.apache.org/ref/3-LATEST/maven-embedder/cli.html)

### Tutorials and Articles

1. 🏢 [Maven Users Centre](https://maven.apache.org/users/index.html)
    1. [Maven in 5 Minutes](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html)
1. 👍 [许晓斌. Maven实战[M]. 机械工业出版社. 2014-07](https://book.douban.com/subject/5345682/)
1. 🌟 [廖雪峰.Java教程-Maven基础[DB/OL]](https://www.liaoxuefeng.com/wiki/1252599548343744/1255945359327200)
1. 👍 [Maven on MacOS (OSX)](https://wilsonmar.github.io/maven-on-macos/)
1. 🌟 [hackjutsu/maven-in-action](https://github.com/hackjutsu/maven-in-action)

### Sources

1. 🏢 [Apache Maven Project](https://maven.apache.org/)
    1. [Apache Maven Source Repository](https://maven.apache.org/scm.html)
    1. [Apache Maven Sources(GitHub mirror)](https://github.com/apache/maven-sources/)
