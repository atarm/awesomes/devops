# 安装与更新Install and Upgrade

## 目录结构

### 安装目录结构

```bash {.line-numbers}
# Maven 3.8.5 in macOS

.
├── bin
│   ├── m2.conf
│   ├── mvn
│   ├── mvnDebug
│   └── mvnyjp
├── boot
│   ├── plexus-classworlds-2.6.0.jar
│   └── plexus-classworlds.license
├── conf
│   ├── logging
│   │   └── simplelogger.properties
│   ├── settings.xml
│   └── toolchains.xml
└── lib
    ├── commons-cli-1.4.jar
    ├── commons-cli.license
    ├── commons-io-2.6.jar
    ├── commons-io.license
    ├── commons-lang3-3.8.1.jar
    ├── commons-lang3.license
    ├── ext
    │   └── README.txt
    ├── guava-25.1-android.jar
    ├── guava.license
    ├── guice-4.2.2-no_aop.jar
    ├── guice.license
    ├── jansi-2.4.0.jar
    ├── jansi-native
    │   ├── README.txt
    │   └── Windows
    │       ├── x86
    │       │   └── jansi.dll
    │       └── x86_64
    │           └── jansi.dll
    ├── jansi.license
    ├── javax.annotation-api-1.2.jar
    ├── javax.annotation-api.license
    ├── javax.inject-1.jar
    ├── javax.inject.license
    ├── jcl-over-slf4j-1.7.32.jar
    ├── jcl-over-slf4j.license
    ├── maven-artifact-3.8.5.jar
    ├── maven-builder-support-3.8.5.jar
    ├── maven-compat-3.8.5.jar
    ├── maven-core-3.8.5.jar
    ├── maven-embedder-3.8.5.jar
    ├── maven-model-3.8.5.jar
    ├── maven-model-builder-3.8.5.jar
    ├── maven-plugin-api-3.8.5.jar
    ├── maven-repository-metadata-3.8.5.jar
    ├── maven-resolver-api-1.6.3.jar
    ├── maven-resolver-connector-basic-1.6.3.jar
    ├── maven-resolver-impl-1.6.3.jar
    ├── maven-resolver-provider-3.8.5.jar
    ├── maven-resolver-spi-1.6.3.jar
    ├── maven-resolver-transport-wagon-1.6.3.jar
    ├── maven-resolver-util-1.6.3.jar
    ├── maven-settings-3.8.5.jar
    ├── maven-settings-builder-3.8.5.jar
    ├── maven-shared-utils-3.3.4.jar
    ├── maven-slf4j-provider-3.8.5.jar
    ├── org.eclipse.sisu.inject-0.3.5.jar
    ├── org.eclipse.sisu.inject.license
    ├── org.eclipse.sisu.plexus-0.3.5.jar
    ├── org.eclipse.sisu.plexus.license
    ├── plexus-cipher-2.0.jar
    ├── plexus-cipher.license
    ├── plexus-component-annotations-2.1.0.jar
    ├── plexus-component-annotations.license
    ├── plexus-interpolation-1.26.jar
    ├── plexus-interpolation.license
    ├── plexus-sec-dispatcher-2.0.jar
    ├── plexus-sec-dispatcher.license
    ├── plexus-utils-3.3.0.jar
    ├── plexus-utils.license
    ├── slf4j-api-1.7.32.jar
    ├── slf4j-api.license
    ├── wagon-file-3.5.1.jar
    ├── wagon-http-3.5.1-shaded.jar
    └── wagon-provider-api-3.5.1.jar

10 directories, 70 files
```

### 重要目录和文件

```bash {.line-numbers}
${M2_HOME}/bin/

${M2_HOME}/conf/settings            # system-scope config

~/.m2/
    ~/.m2/settings.xml              # global-scope config

~/.m2/repository/

~/.m2/wrapper/
```

## Maven Wrapper

![mvn process](./.assets/diagram/mvn.svg)

![mvnw process](./.assets/diagram/mvnw.svg)

## Resources

### `maven-wrapper`

1. 🚫 [takari/maven-wrapper](https://github.com/takari/maven-wrapper)
1. 🏢 [apache/maven-wrapper](https://github.com/apache/maven-wrapper)
