# Multi-Module多模块


>When running mvn site on a top level project, Maven will build all sites for every module defined in the modules section of the pom individually. Note that each site is generated in each individual project's target location. **As a result, relative links between different modules will NOT work.** They will however work when you deploy or stage the site.
>
>>[Building multi-module sites](https://maven.apache.org/plugins/maven-site-plugin/examples/multimodule.html)


1. [Aggregating PMD reports for Multi-Module-Projects](https://maven.apache.org/plugins/maven-pmd-plugin/examples/aggregate.html)
