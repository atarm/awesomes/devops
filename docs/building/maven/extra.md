# Extra

## Third-Party Plugins

### `jacoco`

1. [How To Generate Code Coverage Report Using JaCoCo-Maven Plugin](https://www.lambdatest.com/blog/reporting-code-coverage-using-maven-and-jacoco-plugin/)
1. [JaCoCo report generation using maven](https://stackoverflow.com/questions/22836455/jacoco-report-generation-using-maven)
1. [Maven – JaCoCo code coverage example](https://mkyong.com/maven/maven-jacoco-code-coverage-example/)
1. [Intro to JaCoCo](https://www.baeldung.com/jacoco)

### `Alibaba-P3C`

```xml {.line-numbers}
<configuration>
    <rulesets>
        <ruleset>rulesets/java/ali-comment.xml</ruleset>
        <ruleset>rulesets/java/ali-concurrent.xml</ruleset>
        <ruleset>rulesets/java/ali-constant.xml</ruleset>
        <ruleset>rulesets/java/ali-exception.xml</ruleset>
        <ruleset>rulesets/java/ali-flowcontrol.xml</ruleset>
        <ruleset>rulesets/java/ali-naming.xml</ruleset>
        <ruleset>rulesets/java/ali-oop.xml</ruleset>
        <ruleset>rulesets/java/ali-orm.xml</ruleset>
        <ruleset>rulesets/java/ali-other.xml</ruleset>
        <ruleset>rulesets/java/ali-set.xml</ruleset>
    </rulesets>
</configuration>
```
