# Autotools

## 参考文献Bibliographies

1. Wikipedia. GNU Autotools[DB/OL]. <https://en.wikipedia.org/wiki/GNU_Autotools>. 2021-04-16.
1. Darktea. Autotools使用入门[DB/OL]. <https://darktea.github.io/notes/2012/06/24/autotools.html>. 2012-06-24.
1. 充满活力的早晨. Autotools 工具[DB/OL]. <https://www.jianshu.com/p/b3b0a090a01e>. 2018-12-19.
