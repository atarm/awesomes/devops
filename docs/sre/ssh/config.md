# Secure Shell Protocol

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Files](#files)
2. [Public Key Authentication](#public-key-authentication)
3. [Futhermore](#futhermore)

<!-- /code_chunk_output -->

## Files

1. `server files`
    1. `/etc/ssh/sshd_config`：`sshd`配置文件
    1. `~/.ssh/authorized_keys`：允许被`PubkeyAuthentication`的`public key`
1. `client files`
    1. `~/.ssh/config`：`ssh client`配置文件
    1. `~/.ssh/known_hosts`：`ssh client`连接过的`remote`的信息

## Public Key Authentication

### Multi-Accounts

>1. [SSH的config配置之多账号简单管理](https://jingwei.link/2018/12/15/ssh-config-multi-app-manager.html)

## Futhermore

1. `man sshd`
1. `man sshd_config`
1. [OpenSSH/Client Configuration Files](https://en.wikibooks.org/wiki/OpenSSH/Client_Configuration_Files)
