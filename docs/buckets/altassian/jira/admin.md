# Jira Administrate

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Roles](#roles)

<!-- /code_chunk_output -->

## Roles

从`jira`作为软件的角度看，`jira`有两种角色：

1. `administrator`
1. `user`
