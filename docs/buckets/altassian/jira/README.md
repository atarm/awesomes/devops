# `jira`

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [5W of the `jira`](#5w-of-the-jira)
    1. [Family of `jira` Products](#family-of-jira-products)
    2. [Distributions of `jira`](#distributions-of-jira)
    3. [Alternatives and `SWOT`](#alternatives-and-swot)
        1. [Alternatives](#alternatives)
            1. [`github`/`gitlab` issues](#githubgitlab-issues)
        2. [`SWOT`](#swot)
2. [5W of the User](#5w-of-the-user)
    1. [Conclusion](#conclusion)
        1. [Significant Usages](#significant-usages)
3. [How](#how)
    1. [`jira-server` and `jira-cloud`](#jira-server-and-jira-cloud)
    2. [Clients](#clients)
    3. [Plans and Pricing](#plans-and-pricing)
        1. [Free vs Standard](#free-vs-standard)
    4. [Key Terminologies](#key-terminologies)
4. [Futhermore](#futhermore)
    1. [Tutorials](#tutorials)
    2. [Documents and Supports](#documents-and-supports)

<!-- /code_chunk_output -->

## 5W of the `jira`

`jira`这个词来自日语单词，即`"Gojira"`（哥斯拉）。

`jira`最初主要用于软件测试领域的`bug tracking`，目前主要用于基于`issue tracking`的`software project management`，以`jira`为中心前向集成`confluence`，后向集成`bitbucket`、`jira service management`、`opsgenie`可以实现软件项目的全生命周期管理

`jira`的核心结构是`project-issue-board`，其中`issue`是核心中的核心，`project`是`issue`的集合，`board`是`issue`的视图。

`issue workflow`和基于强大集成能力的`issue status automation`是`jira`最大的优势，其中强大的集成能力又是基于其庞大的`plugin ecosystem`（超过1000个用于`jira`的插件，数据来源于[atlassian.com. Jira Software Cloud vs. Azure DevOps.](https://www.atlassian.com/software/jira/comparison/jira-vs-azure-devops)）

### Family of `jira` Products

1. `Jira Software` is built specifically for software teams. Jira Software combines powerful developer tool integrations with the most important features and functionality required for great agile software development.
1. `Jira Service Management` is built for IT and service teams, providing them with everything they need for out-of-the-box incident, problem, and change management.
1. `jira work management`(原`Jira Core`) is a simplified project management tool for customers looking to extend the power of Jira to their organization.

### Distributions of `jira`

1. `jira-server`: **end of support after 2024-02-15**
1. `jira-cloud`
1. `jira-datacenter`

>1. [atlassian.com. Moving to a Cloud future, together.](https://www.atlassian.com/migration/assess/journey-to-cloud)

### Alternatives and `SWOT`

#### Alternatives

>1. [atlassian.com. Jira Software Alternatives.](https://www.atlassian.com/software/jira/comparison)

##### `github`/`gitlab` issues

>1. [gitlab.com. GitLab vs Jira Overview.](https://about.gitlab.com/devops-tools/jira-vs-gitlab/)

#### `SWOT`

## 5W of the User

### Conclusion

#### Significant Usages

## How

### `jira-server` and `jira-cloud`

### Clients

1. `jira-cloud for web`
1. `jira-cloud for mobile`: offer `for android` and `for ios`
1. `ide plugin`
    - `jetbrains`: 
    - `vscode`: 

>Sunsetting the Jira Cloud for Mac app. Jira Cloud for web & mobile continues to accelerate.
>
>>[Announcement: Sunsetting the Jira Cloud for Mac App](https://community.atlassian.com/t5/Jira-Mobile-Apps-articles/Announcement-Sunsetting-the-Jira-Cloud-for-Mac-App/ba-p/1911778)

### Plans and Pricing

>1. [plans and pricing](https://www.atlassian.com/software/jira/pricing)

#### Free vs Standard

||free|standard|
|:--|:--:|:--:|
|user limit|10|20,000|
|project roles|❎|✅|
|advanced permissions|❎|✅|
|audit logs|❎|✅|
|anonymous access|❎|✅|
|data residency|❎|✅|
|file storage|2GB|250GB|

❗ 免费版所有成员都是`administrator`权限 ❗

### Key Terminologies

1. `agile`: a project management methodology. It takes an iterative approach to managing projects, focusing on continuous release that require regular updates based on feedback. Agile helps teams increase delivery speed, expand collaboration, and better respond to market trends
1. `scrum`: a specific agile project management framework to help design, develop, and deliver complex products. It's mostly used by software development teams. Scrum teams organize their work in sprints. A sprint is a short, fixed period when a scrum team works to complete a batch of issues
1. `project`: a project is a collection of `issues`, `projects` can be organized by teams or larger deliverables
1. `issue`: an `issue` an individual work item
    1. `issue card`: `issue card` is a look-like when issue is shown on a `board`
    1. `status`: a `status` shows the current progress of an issue. Common statuses are "To Do, In Progress, In Review, Done."
    1. `workflow`: a `workflow` is the path of `statuses` an `issue` will go through from start to finish
1. `board`: a `board` is a visual display of working progress (`sprint` when uses `scrum`), often with 3-4 columns
    1. `kanban board`: `kanban boards` show a continuous flow of work, `issues` come in and out of the board from start to finish.
    1. `scrum board`: `scrum boards` bring in groups of issues that are worked on during a fixed period of work time, often a two-week "sprint."

## Futhermore

### Tutorials

1. 👑 [atlassian.com. Learn Jira with Atlassian University.](https://university.atlassian.com/student/path/871316-learn-jira-with-atlassian-university?utm_source=jira-help&utm_medium=inapp&utm_campaign=P:uni-training*O:university*I:in-app-help*)
    1. [Jira Fundamentals](https://university.atlassian.com/student/collection/850385/path/1083901)
    1. [Jira Service Management Fundamentals](https://university.atlassian.com/student/collection/850385/path/1277309)
1. 👑 [atlassian.com. Jira tutorials: Learn agile with Jira Software.](https://www.atlassian.com/agile/tutorials)
1. [tutorialspoint.com. JIRA Tutorial.](https://www.tutorialspoint.com/jira/index.htm)

### Documents and Supports

1. 👑 [atlassian.com Jira Software Cloud support.](https://support.atlassian.com/jira-software-cloud/)
