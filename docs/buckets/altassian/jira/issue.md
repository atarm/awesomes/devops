# Jira Issue

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Overview](#overview)
2. [Types](#types)
3. [Hierarchy](#hierarchy)
4. [Relationship between Issues](#relationship-between-issues)
5. [Statuses](#statuses)
6. [Status Transition](#status-transition)
7. [Workflow and Automation](#workflow-and-automation)
8. [Screens](#screens)
9. [Attributes](#attributes)
    1. [Labels](#labels)

<!-- /code_chunk_output -->

## Overview

`issue types`、`issue hierarchy`、`issue status`是关键。

在`jira`，理论中的`agile`的`epic`、`feature`、`story`、`task`都是`issue`的类型，并形成`epic - story - sub-task`三层结构，没有明确区别`feature`和`story`，其中`epic`和`sub-task`是预定义有特殊用途的`issue`类型，`story`可以自定义扩充不同的`story`类型。

## Types

>1. [atlassian. Create an issue and a sub-task.](https://support.atlassian.com/jira-software-cloud/docs/create-an-issue-and-a-sub-task/)

## Hierarchy

`epic - story - sub-task`

>1. [What's Epic, User Story and Task in Scrum Work Hierarchy](https://adaptmethodology.com/epic-user-story-task/)

## Relationship between Issues

1. `vertical-relationship`
1. `horizontal-relationship`

## Statuses

## Status Transition

1. 自动迁移
    1. 通过配置`automation`，根据行为隐性地进行状态迁移
    1. 行为显式地指定状态迁移
1. 手动迁移：在`jira`页面上手动地对`issue status`进行迁移

>1. [atlassian support. Use Smart Commits.](https://support.atlassian.com/bitbucket-cloud/docs/use-smart-commits/)

## Workflow and Automation

1. [atlassian support. Configure workflow triggers.](https://support.atlassian.com/jira-cloud-administration/docs/configure-workflow-triggers/)
1. [atlassian support. Manage how work flows in your team-managed project.](https://support.atlassian.com/jira-software-cloud/docs/manage-how-work-flows-in-your-team-managed-project/)

## Screens

## Attributes

### Labels

`jira`没有提供一个单一的`label`管理界面，而是通过分散在各个`issue`和`JQL`进行操作。

>You cannot manage labels centrally, it can only be done via issues. For example if you need to rename one you would have do a JQL to select all issues with the label, bulk edit them and add the new label, then bulk edit again to remove the old label.
>
>Another option would be to use one of the apps available to manage labels. Just search on labels in the Marketplace.
>
>>[Mikael Sandberg. Where can I manage labels?](https://community.atlassian.com/t5/Jira-questions/Where-can-I-manage-labels/qaq-p/665298)

>1. [atlassian.com. How to edit/rename labels in Jira issues.](https://confluence.atlassian.com/jirakb/how-to-edit-rename-labels-in-jira-issues-1116305331.html)
>1. [atlassian.com. How to delete a label in Jira.](https://confluence.atlassian.com/jirakb/how-to-delete-a-label-in-jira-297667646.html)
