# GitLab

## Useful Concepts

### Group

### Project Templates

### Push Rules (💰 `PREMIUM`)

>1. [gitlab.com. Push rules.](https://docs.gitlab.com/ee/user/project/repository/push_rules.html)

### Merge Request Approval Rules (💰 `PREMIUM`)

>1. [gitlab.com. Merge request approval rules.](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/rules.html)

## 使用技巧Use Skills

1. `issue`的`Description`中插入`Label`： `~${label_name}`，即，输入`~`后自动提示可选的`label_name`（`GitLab Community`只显示至多5个`label_name`供选择，可输入关键字进行自动匹配筛选）

## 延伸阅读Futhermore

1. DevOps
    1. DevOps Tools Landscape: <https://about.gitlab.com/devops-tools/>
1. gitLab documentation guidelines: <https://repository.prace-ri.eu/git/help/development/documentation/index.md>
1. the gitlab team handbook: <https://about.gitlab.com/handbook/>
    1. GitLab Handbook Usage: <https://about.gitlab.com/handbook/handbook-usage/>
1. gitLab culture: <https://about.gitlab.com/company/culture/>
