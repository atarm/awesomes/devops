# Awesome Script Drawing


## Script Drawing Tools

>https://github.com/SMUsamaShah/SMUsamaShah.github.io/blob/master/_posts/2020-06-07-text-to-diagram-tools.md

A comprehensive list of various text to diagram tools.

- **Markdeep** <https://casual-effects.com/markdeep/>
: Diagrams: markdown, ascii-to-all

- **Svgbob** <https://ivanceras.github.io/svgbob-editor/>
: Diagrams: ascii-to-all

- **Archetype** <https://fatiherikli.github.io/archetype/>
: Diagrams: ascii-drawing

- **Textik** <https://textik.com/>
: Diagrams: ascii-drawing

- **ASCIIFlow** <http://asciiflow.com/>
: Diagrams: ascii-drawing

- **BPMN Sketch Miner** <https://www.bpmn-sketch-miner.ai/>
: Diagrams: BPMN/flow

- **Webgraphviz** <http://www.webgraphviz.com/>
: Diagrams: graphviz

- **Edotor** <https://edotor.net/>
: Diagrams: graphviz

- **nomnoml** <http://www.nomnoml.com/>
: Source: <https://github.com/skanaar/nomnoml>
: Diagrams: activity, class

- **CodeUML** (dead) <http://codeuml.com/> 
: Source: <https://github.com/oazabir/codeuml>
: Diagrams: uml

- **yuml** <https://yuml.me/diagram/scruffy/class/draw>
: Diagrams: uml

- **PlantText** <http://www.planttext.com/planttext>
: Diagrams: activity, class, component, object, use case, state, graphviz, sequence, gui, earth

- **PlantUML** <http://www.plantuml.com/plantuml/uml/>
: Diagrams: activity, class, component, object, use-case, sequence, state, deployment, timing, gui, archimate, gantt, mindmap, tree, math, entity-relationship

- **Umple Online** <http://cruise.site.uottawa.ca/umpleonline/>
: Diagrams: class, state, composite

- **ZenUML** <https://app.zenuml.com/>
: Diagrams: sequence

- **DotUML** <https://dotuml.com/playground.html>
: Diagrams: class, use case, sequence, state, deployment, graphviz

- **dbdiagram** <https://dbdiagram.io/home>
: Diagrams: sequence

- **WebSequenceDiagrams** <https://www.websequencediagrams.com/>
: Diagrams: sequence

- **SVG Sequence Diagram** <http://sullerandras.github.io/SVG-Sequence-Diagram/>
: Diagrams: sequence

- **JUMLY** (dead) <http://jumly.tmtk.net>
: Source: <https://github.com/tmtk75/jumly>
: Diagrams: sequence

- **js sequence diagrams** <http://bramp.github.io/js-sequence-diagrams/>
: Diagrams: sequence

- **swimlanes** <https://swimlanes.io/>
: Diagrams: sequence

- **SequenceDiagram** <https://sequencediagram.org/>
: Diagrams: sequence

- **GraphUp** (broken) <https://graphup.co/>
: Diagrams: sequence, gantt

- **Text Diagram** <http://weidagang.github.io/text-diagram/>
: Diagrams: ascii sequence

- **ASCII Sequence Diagram Creator** <https://textart.io/sequence>
: Diagrams: ascii sequence

- **Chart Mage** <http://chartmage.com/index.html>
: Diagrams: sequence, flow

- **flowchart.js** <http://flowchart.js.org/>
: Diagrams: flow

- **drawthe** <http://go.drawthe.net/>
: Source: <https://github.com/cidrblock/drawthe.net>
: Diagrams: network

- **mermaid** <https://mermaidjs.github.io>
: Diagrams: flow, sequence, class, state, entity-relationship, gantt, pie, journey

- **Diagram.codes** <https://www.diagram.codes/>
: Diagrams: flow, sequence, graph, tree, onion, stack, system, timeline, mindmap

- **Blockdiag** <http://interactive.blockdiag.com/>
: Diagrams: block, activity, network, rack/stack, packet

- **MetaUML** (dead) <https://metauml.denksoft.com>
- **Railroad Diagram Generator** <http://www.bottlecaps.de/rr/ui#_StringLiteral>
: Diagrams: railroad/syntax

- **Gleek** <https://www.gleek.io/>
: Diagrams: architecture

- **Kroki** <https://kroki.io/>
- **Penrose** <https://github.com/penrose/penrose>
: Diagrams: penrose

- **code2flow** <https://app.code2flow.com/>
: Diagrams: flow

- **WaveDrom** <https://wavedrom.com/editor.html>
: Diagrams: timing


CLI Tools

- **perl graph-easy** <http://bloodgate.com/perl/graph/index.html>
: Diagrams: flow
: Doc: <https://developpaper.com/recommend-a-tool-for-making-ascii-flow-chart-graph-easy>
: Example: graph-easy <<< [node 1]->[some text]-[this][node 1]->[branch]