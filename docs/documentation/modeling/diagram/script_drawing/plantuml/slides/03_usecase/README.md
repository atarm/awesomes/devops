---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# Usecase Diagram

<!-- slide -->
# Define UseCase

1. 定界符定义法：**`(<usecase_description>)`**
1. **`usecase (<usecase_description>)`：** 如果 **`usecase_description`** 中不含空白字符，**`()`** 可省略
1. **`usecase "<usecase_description>"`：** 如果 **`usecase_description`** 中不含空白字符，**`""`** 可省略

<!-- slide -->
# Define Alias of UseCase

+ **`<define usecase> as <alias_name>`**

<!-- slide -->
# Define Multi-line Descriptions with Separators

+ **`usecase <alias_name> as "<multi-line descriptions with separators>"`：** , 分隔符定义如下：
    1. **`--`**, **`==`**, **`..`**, **`__`**
    1. **`--<title>--`**, **`==<title>==`**, **`..<title>..`**, **`__<title>__`**

<!-- slide -->

@import "../../resources/sample/usecase_define_usecase.puml"

<!-- slide -->
# Define Actors

+ 与定义用例的用法一

<!-- slide -->
# Connect UseCase and Actor

+ 连接用例和参与者： **`-`（实线）或`.`（虚线）** ，单符号与全局方向成垂直，两个或两个以上符号与全局方向成水平，且符号越多连线越长
    1. **`-->`或`..>`：** 有箭头，与图示全局方向成水平
    1. **`->`或`.>`：** 有箭头，与图示全局方向成垂直
    1. **`--`或`..`：** 无箭头，与图示全局方向成水平
    1. **`-`或`.`：** 无箭头，与图示全局方向成垂直
+ **`<actor> --> <usecase> : <label>`：** **`<label>`** 表示连线上的文本标签

<!-- slide -->
# `<<Extends>>`, `<<Include>>` and Generalization Relationship

1. **`<<Extends>>`, `<<Include>>`：** 使用连线的文本标签
1. **Generalization：** 使用 **`--|>`** 连线符号

<!-- slide -->

@import "../../resources/sample/usecase_rationships.puml"

<!-- slide -->
# System Boundary

```{.line-numbers}
rectangle <"name">{
    ....
}
```

<!-- slide -->

@import "../../resources/sample/usecase_boundary.puml"

<!-- slide -->
# Stereotypes

**`<<stereotype name>>`** 定义用例或参与者的 **stereotype（版型）**

<!-- slide -->

@import "../../resources/sample/usecase_stereotype.puml"

<!-- slide -->
# Arrows Direction

<!-- slide -->

@import "../../resources/sample/usecase_arrow_direction.puml"

<!-- slide -->
# Diagram Direction

<!-- slide -->

@import "../../resources/sample/usecase_diagram_direction.puml"

<!-- slide -->
# Thank You

## :ok:End of This Slide:ok:
