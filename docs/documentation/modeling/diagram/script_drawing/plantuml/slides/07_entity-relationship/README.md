---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# Entity Relationship Diagram

<!-- slide -->
# Entities

<!-- slide -->

@import "./00_Diagram/demo_erd.puml"
