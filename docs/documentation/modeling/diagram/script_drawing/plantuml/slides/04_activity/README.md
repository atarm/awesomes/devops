---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# Activity Diagram

<!-- slide -->
# Activity and Arrow

1. **`:<activity labels>;`** `<activity labels>`中可包含空白、可换行、可使用 **`creole wiki syntax`** 格式化字符样式
1. Activity间按定义的先后顺序自动绘制Arrow，也可通过在Activity中间定义 **`->`** 自定义Arrow样式
1. **`start`** 、**`stop`** 、**`end`** 三个特殊的Activities

<!-- slide -->

@import "../../resources/sample/activity_arrow.puml"

<!-- slide -->
# `if-then-elseif-else-endif` Conditional

@import "../../resources/sample/activity_if.puml"

<!-- slide -->
# `repeat-repeat while` Loop

@import "../../resources/sample/activity_repeat.puml"

<!-- slide -->
# `while-endwhile` Loop

@import "../../resources/sample/activity_while.puml"

<!-- slide -->
# Swimlanes

@import "../../resources/sample/activity_swimlane.puml"

<!-- slide -->
# Thank You

## :ok:End of This Slide:ok:
