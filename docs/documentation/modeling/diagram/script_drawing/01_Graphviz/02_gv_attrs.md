# Graphviz常用属性

<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->
<!-- code_chunk_output -->

1. [Graphviz的属性语法](#graphviz的属性语法)
2. [graphviz的颜色属性](#graphviz的颜色属性)
3. [GSCNE常用属性](#gscne常用属性)
4. [仅用于graph的常用属性](#仅用于graph的常用属性)
5. [仅用于subgraph的常用属性](#仅用于subgraph的常用属性)
6. [与subgraph有关的属性](#与subgraph有关的属性)
7. [仅用于Node的常用属性](#仅用于node的常用属性)
8. [仅用于Edge的常用属性](#仅用于edge的常用属性)
9. [帮助文档](#帮助文档)

<!-- /code_chunk_output -->

## Graphviz的属性语法

1. **属性界定符：** `Graph/Subgraph`的属性使用单独的语句直接定义，`Node`和`Edge`的属性用`[]`来界定属性，属性之间用`,`或`;`分隔
1. **属性值引号可选：** 属性值的引号是可选的（如果属性值包含空格，则必须使用引号）
1. **属性大小写敏感：** 属性名大小写敏感、属性值也大多数大小写敏感，属性名除`Damping`（仅用于`neato`布局）、`colorList`、包含`URL`的属性（仅用于`svg`、`postscript`、`map`输出格式）外，其他属性名为全小写
1. **属性值转义字符：** 属性值可以使用特殊转义字符（与`C/CPP`的特殊转义字符一致）
1. **属性值度量单位：** 属性值度量单位使用`inches`（72 points/inch）

在`Graphviz`中，即可以定义多个`Graph/Subgraph`、`Node`或`Edge`公共属性，也可以定义单个实体的特殊属性。

```{.line-numbers}
digraph ex_attr{
    bgcolor=transparent;
    node[shape=box];    //定义Node的公共属性，属性有效区域直至公共属性被覆盖或超过代码块作用域
    edge[color=red];    //定义Edge的公共属性，属性有效区域直至公共属性被覆盖或超过代码块作用域

    demo_node_01;                   //应用上面定义的Node公共属性
    demo_node_02[shape=ellipse];    //定义demo_node的特殊属性

    demo_node_01 -> demo_node_02;   //应用上面定义的Edge公共属性
    demo_node_01 -> demo_node_03[color=blue];
    
    node[shape=plaintext];          //覆盖上面定义的Node公共属性
    demo_node_02 -> demo_node_04;
}
```

<!-- @import "./00_Diagram/05_ex_attrs.gv" -->

## graphviz的颜色属性

`graphviz`支持三种方式定义颜色：

1. 颜色名：Graphviz定义的颜色名，颜色名大小写不敏感并且忽略非字母字符（比如`warmgery`和`Warm_Grey`是等价的），详见[Color Names](https://graphviz.gitlab.io/_pages/doc/info/colors.html)
1. HSB颜色：3个$[0,1]$中间用`,`分隔的浮点数
1. RGB颜色：以`#`开头的3个十六进制数字或4个十六进制数字，即`#RGB`或`#RGBA`

## GSCNE常用属性

1. **`label`：** 可应用于`GCNE`，`label`属性值可以是`HTML`语句
1. **`labelloc`：** 可应用于`GCN`
1. **`style`：** 可应用于`CNE`，详细解释参见<https://graphviz.gitlab.io/_pages/doc/info/attrs.html#k:style>
    + **`Cluster`：** `enum{solid, dashed, dotted, bold, rounded, filled, striped}`
    + **`Node`：** `enum{solid, dashed, dotted, bold, rounded, diagonals, filled, striped, wedged}`
    + **`Edge`：** `enum{solid, dashed, dotted, bold}`
1. **`color`：** 可应用于`CNE`，边框色
1. **`fillcolor`：** 可应用于`ENC`，填充色，节点和聚集子图应设置属性`style="filled"`
1. **`bgcolor`：** 可应用于`GC`
1. **`fontcolor`：**
1. **`penwidth`：** 描绘节点边框、连线的宽度
1. **`nojustify`：** 

## 仅用于graph的常用属性

1. **rankdir：** 排列方向，`enum{LR, TB}, default=TB`
1. **splines：** 设置连线的线的形状，`enum{none|"",line|false,spline|true,polyline,curved,ortho}, default=false`
1. **center：** 居中绘制，`bool, default=false`
1. **compound：** 是否允许连线头尾端落在子图中，需配合`edge`的`lhead`和`ltail`属性使用，`bool, default=false`
1. **nodesep：**
1. **ranksep：** 仅应用于使用`dot`和`twopi`布局时

## 仅用于subgraph的常用属性

**`rank`：** `enum{same, min, max, source, sink}`

**:warning: `subgraph`只可应用`rank`一个属性，`rank`也仅能应用于`subgraph`。**

## 与subgraph有关的属性

有三个属性不是`subgraph`的属性，但实际上是应用于`subgraph`：

1. **`compound`：** `G`属性（`dot only`），是否允许连线头尾端落在子图中，配合`E`的`lhead`和`ltail`属性使用，`bool, default=false`
1. **`lhead`：** `E`属性（`dot only`），连线头端指向的子图（`G`的`compound=true`时有效）
1. **`ltail`：** `E`属性（`dot only`），连线尾端指向子图（`G`的`compound=true`时有效）

## 仅用于Node的常用属性

1. **`fixedsize`：** 设置`N`尺寸是否为固定大小（即： `label`长短是否 **不影响** `Node`尺寸），`bool, default=false`
1. **`image`：** 节点背景图片
1. **`shape`：** 节点形状，**`default=ellipse`**，常用的基本形状有 **`ellipse`, `oval`, `box`, `rect`, `rectangle`, `none`, `underline`**，常用的特殊形状有 **`cylinder`, `note`, `tab`, `folder`, `box3d`, `component`**

## 仅用于Edge的常用属性

1. **`dir`：** 连线的箭头所在位置，`enum{forward, back, both, none}, default="forward"`
1. **`arrowhead`：** 连线头端形状
1. **`arrowtail`：** 连线尾端形状
1. **`arrowsize`：** 连线端形状大小
1. **`decorate`：** 连线文本下划线装饰，`bool, default=false`

## 帮助文档

1. **完整的属性清单帮助文档：** https://graphviz.gitlab.io/_pages/doc/info/attrs.html
1. **完整的Node形状帮助文档：** https://graphviz.gitlab.io/_pages/doc/info/shapes.html
1. **完整的Edge箭头形状帮助文档：** https://graphviz.gitlab.io/_pages/doc/info/arrows.html
1. **完整的Color帮助文档：** https://graphviz.gitlab.io/_pages/doc/info/colors.html