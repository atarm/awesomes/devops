# 使用Graphviz自动化绘制图示

<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->
<!-- code_chunk_output -->

1. [Graphviz是什么？](#graphviz是什么)
2. [为什么要使用Graphviz？](#为什么要使用graphviz)
3. [使用Graphviz的方法](#使用graphviz的方法)
4. [Graphviz的基本组成结构和使用流程](#graphviz的基本组成结构和使用流程)
5. [Graphviz的MacOS独立客户端](#graphviz的macos独立客户端)
6. [延伸阅读](#延伸阅读)
7. [本文图示的脚本源码](#本文图示的脚本源码)
    1. [Grpahviz基本组成结构的dot脚本源码](#grpahviz基本组成结构的dot脚本源码)
    2. [Graphviz使用一般流程的PlantUML源码](#graphviz使用一般流程的plantuml源码)

<!-- /code_chunk_output -->

## Graphviz是什么？

![Graphviz_logo](./00_Images/Graphviz_logo.png)

`Graphviz（Graph Visualization）`是1990年代初诞生于`AT&T`的`Bell实验室`的一个开源的 **（EPL授权）** 、**跨平台** 的 **脚本自动化绘图软件工具**。

`Graphviz`是基于`dot`脚本语言的自动绘图软件，`dot`是一种`DSL(Domain Special Language)`脚本语言。

`Graphviz`使用一种称为`dot`语言的`DSL(Domain Special Language)`语言编写`Script File脚本文件`，然后使用`Layout布局引擎`解析这个`Script File脚本文件`完成 **自动化布局渲染** 。

>`AT&T`是美国三大的电信运营商之一，现在美国的电信运营商行业也是从原`AT&T`被强行支解后再不断合并后形成群雄逐鹿的竞争局面。
>`AT&T`的前身是`Bell电话公司`，`Bell电话公司`是发明电话的公司。
>在IT软件世界中，`Bell实验室`创造了`Unix`、`C语言`等一系列伟大的发明。

`Graphviz`脚本文件一般以`.gv`或`.dot`为文件扩展名，由于`Microsoft Office Word`模板文件的扩展名也是`.dot`，为避免冲突，一般建议`Graphviz`脚本文件的扩展名保存为`.gv`。

>**备注： 在Graphviz中，`dot`这个词主要有三种不同的用途：**

>1. `Graphviz`的`Script脚本`的语言名称/语法名称
>1. `Graphviz`的 **其中一种** `Layout`名称，同时也是 **其中一种** `CLI`命令名称 
>1. `Graphviz`的 **其中一种** `Script File（脚本文件）`常用扩展名

## 为什么要使用Graphviz？

1. **自动排版效率更高：** `Graphviz`主要用于绘制“关系图”，`Graphviz`自动排版以使“最小化连线交叉”，`Graphviz`的自动排版比“所见即所得”的绘画软件（如：`Omnigraffle`、`Microsoft Office Visio`等）
1. **文本文件管理更方便：** `VCS(Version Control System版本控制系统)`对“所见即所得”的绘画软件生成的文件无法有效地进行版本管理，而文本文件则可以在`VCS`中有效地被进行版本管理起来
1. **其他自动化绘图工具的基础：** `Graphviz`是其他自动化绘图工具的基础（如`PlantUML`等），也是很多`Data Visualization数据可视化`工具的基础，有点类似于`Python`中`Matplotlib`的作用

优点
相对于ms viso需要手动拖动图标，graphviz只需要通过DOT代码表达各节点的逻辑关系，然后自动布局和输出关系图。最小化复杂关系的连线交叉，增强可读性。

尤其在绘制非常复杂的逻辑关系、组织架构、模块组成、数据结构等图时，可以大大减少工作量，理清思路。


缺点
无法绘制需要自定义或者固定布局的图，比如时序图。

## 使用Graphviz的方法

扩展名
Graphviz支持两种文件扩展名：*.gv和*.dot，使用*.gv是为了防止与早期的ms word的扩展名冲突。
后文中的DOT文件即表示输入graphviz处理的文件。


输出格式
Graphviz支持的输出文件格式：

图片格式：bmp、png、gif、ico、jpg、jpeg、svg
文档格式：pdf、ps

## Graphviz的基本组成结构和使用流程

`Graphviz`的基本组成结构包括`Layout自动化布局工具`和`Script脚本文件`两部分。

`Script脚本文件`主要包括`Elements实体`和`Attributes属性`两部分。

`Elements实体`主要包括`Graph图`、`Node节点`、`Edge连线`三种类型。

>**备注： 如果需要在软件中调用`Graphviz`， `Graphviz`还提供了`C/CPP`、`Java`、`Python`、`php`等语言的`API`。**

<!-- @import "./00_Diagram/01_gv_basic_structure.gv" -->

`Graphviz`使用起来也非常简单方便，其一般流程如下：
<!-- @import "./00_Diagram/02_gv_basic_use_process.puml"-->

## Graphviz的MacOS独立客户端

在`MacOS 10.6`之前由[Glen Low开发的MacOS X GUI版本](http://www.pixelglow.com/graphviz/)于2004年获得了两项苹果设计奖（ **"Best Product New to Mac OS X Runner-Up"** 和 **"Best OS X Open Source Product 2004 Winner"** ），当时的`MacOS X`基于`PowerPC CPU`，该`Graphviz GUI版`自2004年08月23日发布支持`MacOS 10.3`的1.13版本之后该就没有再更新，即：该`Graphviz GUI版`不能在当今基于`Intel CPU`的macOS中运行。。

>MacOS 10.2和10.3基于`PowerPC CPU`，2005年发布的10.4开始同时基于`Intel CPU`和`PowerPC CPU`，2009年发布的10.6不再支持`PowerPC CPU`。

**目前暂未发现有支持`MacOS X 10.6`以后的独立`GUI客户端`，`MacOS X 10.6`以后可以使用`CLI`或在其他内嵌`Graphviz`工具中使用（比如：在[ATOM](https://atom.io/)编辑器中使用`markdown-preview-enhanced`插件中可嵌入`Graphviz Script File`）。**

## 延伸阅读

1. `Graphviz`的官网链接（英文）：http://www.graphviz.org/
1. `Graphviz`的维基百科（英文）：https://en.wikipedia.org/wiki/Graphviz
1. `Graphviz` Documentation（英文）：http://graphviz.org/documentation/
1. `Grpahviz`示例： http://www.graphviz.org/gallery/
1. Drawing Graphs using Dot and Graphviz（英文）：http://www.tonyballantyne.com/graphs.html
1. Drawing Graphs using Dot and Graphviz（中文翻译）：(https://casatwy.com/shi-yong-dotyu-yan-he-graphvizhui-tu-fan-yi.html
1. GraphViz Pocket Reference（英文）：(https://graphs.grevian.org/example)
1. Graphviz简易教程（中文）：https://blog.zengrong.net/post/2294.html

## 本文图示的脚本源码

### Grpahviz基本组成结构的dot脚本源码

```{.line-numbers}
digraph gv_basic_structure{
    label=<<B>Graphviz基本组成结构</B>>;
    labelloc=t;
    bgcolor=transparent;
    
    node[shape=box];
    //edge[style=bold];
    
    graphviz[label="Graphviz"];
    
    subgraph{
        layout[label="Layouts"];
        script[label="Script Files"];
        api[label="APIs"]
        rank=same;
    }
    
    graphviz -> layout;
    graphviz -> script;
    graphviz -> api;
    
    
    script ->
    subgraph{
        element[label="Elements"];
        attribute[label="Attributes"];
        rank=same;
    }
    
    layout ->
    subgraph{
        layout_etc[label="......"];
        layout_dot[label="dot"];
        layout_neato[label="neato"];
    }
    
    element ->
    subgraph{
        ele_graph[label="Graph"];
        ele_node[label="Node"];
        ele_edge[label="Edge"];
    }
}
```

### Graphviz使用一般流程的PlantUML源码
```{.line-numbers}
@startuml
start
:定义Graph属性;
:定义Node、Edge默认属性;
:添加Node和Edge;
:定义特定Node、Edge的个性属性;
:使用CLI或GUI布局引擎工具渲染绘制;
end
@enduml
```

