# GraphViz布局

<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->
<!-- code_chunk_output -->

1. [布局的作用](#布局的作用)
2. [布局的类型](#布局的类型)
3. [有向图布局](#有向图布局)

<!-- /code_chunk_output -->


## 布局的作用
Graphviz的布局的作用是：按算法将顶点自动分布到画布上，并尽可能地减速少边的交叉（如果交叉过多，将难以看清楚顶点之间的关系）。

## 布局的类型

Graphviz提供的布局包括：

1. **`dot`：** 主要用于`有向图`，杉山型层级布局（Sugiyama-style hierarchical Layout）[^offical_describe_dot_layout]，默认布局，图形具有明确的绘制方向
1. **`neato`：** 主要用于`无向图`，`Force-Based Spring-Model Layout`，用于100个节点的图形，图形没有方向性
1. **`fdp`：** 主要用于`无向图`，`Spring-Model Layout`（Mac OS的版本称为“energy minimised”）
1. **`sfdp`：** 主要用于`大型无向图`（需要放大到很大尺寸的无向图），multiscale version of fdp for the layout of large graphs
1. **`twopi`：** 放射型布局/径向布局
1. **`circo`：** 圆环型布局
1. **`osage`：** 紧凑集群布局

**:warning: 注意：fdp和sfdp不适合有向图。**

[^offical_describe_dot_layout]: 官方描述`dot`布局为 "hierarchical" or "layered" drawings of directed graphs

图形绘制人员使用dot脚本定义图形元素，然后选择算法进行布局，最终导出结果。

Graphviz采用“脚本定义、自动布局”方式的优点是效率高，缺点是无法对元素进行详细控制，因此对于具体的任务来说存在取舍的问题。

## 有向图布局

```dot
digraph G { 
    main -> parse -> execute; 
    main -> init; 
    main -> cleanup; 
    execute -> make_string; 
    execute -> printf 
    init -> make_string; 
    main -> printf; 
    execute -> compare; 
}
```

```dot{engine="circo"}
digraph G { 
    main -> parse -> execute; 
    main -> init; 
    main -> cleanup; 
    execute -> make_string; 
    execute -> printf 
    init -> make_string; 
    main -> printf; 
    execute -> compare; 
}
```

```dot{engine="neato"}
graph G { 
    main -- parse -- execute; 
    main -- init; 
    main -- cleanup; 
    execute -- make_string; 
    execute -- printf 
    init -- make_string; 
    main -- printf; 
    execute -- compare; 
}
```

```dot{engine="twopi"}
digraph G { 
    main -> parse -> execute; 
    main -> init; 
    main -> cleanup; 
    execute -> make_string; 
    execute -> printf 
    init -> make_string; 
    main -> printf; 
    execute -> compare; 
}
```

```dot{engine="osage"}
digraph G { 
    main -> parse -> execute; 
    main -> init; 
    main -> cleanup; 
    execute -> make_string; 
    execute -> printf 
    init -> make_string; 
    main -> printf; 
    execute -> compare; 
}
```