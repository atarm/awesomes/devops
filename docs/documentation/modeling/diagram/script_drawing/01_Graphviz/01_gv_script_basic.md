# Graphviz脚本基本语法
<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->
<!-- code_chunk_output -->

1. [Graphviz的脚本语法结构](#graphviz的脚本语法结构)
2. [Graphviz的三种实体对象和属性](#graphviz的三种实体对象和属性)
3. [Grpahviz图实体](#grpahviz图实体)
4. [Grpahviz节点实体之基本节点](#grpahviz节点实体之基本节点)
5. [Grpahviz节点实体之HTML节点](#grpahviz节点实体之html节点)
6. [Graphviz节点实体之Record节点](#graphviz节点实体之record节点)
7. [Graphviz的连线实体](#graphviz的连线实体)

<!-- /code_chunk_output -->

## Graphviz的脚本语法结构

`Graphviz`支持`digraph（direction graph）有向图`和`graph无向图`两种图形，脚本的语法非常简单，并且与`C/CPP`语法类似：

1. **`代码块（图或子图）`：** `{}`包含的语句是代码块
1. **`语句`：** 语句以`;`结尾（可选）、也 **不必强制换行** （但为可读性建议换行并以`;`结尾），语句有：`代码块语句`、`节点语句`、`连线语句`、`属性语句`四种
1. **`实体对象标识符`：** 可以是`C/CPP标识符`、`数字`、`字符串`（中文字符串等非英文字符串也可以）、`用单引号或双引号包含的字符串`（用于有`标点字符`或`空白字符`的字符串），总的来说，`Graphviz`的`实体对象标识符`除了`特殊字符`外均是合法的标识符
1. **`注释`：** `//`表示单行注释，`/*...*/`表示多行注释

## Graphviz的三种实体对象和属性

`Graphviz`有三类实体对象，在这些实体上可以定义属性（如：颜色、形状、文本等）：

1. **`G - graph（图）`：** `Graph`类似于`Container（容器）`的作用，用于容纳`N`和`E`
1. **`N - nodes（结点）`：** 相同的标识符被视为同一个节点
1. **`E - edges（连线）`：** 节点之间的连线

`Graphviz`的实体对象均在首次定义（即：首次出现）时被创建。

## Grpahviz图实体

`Graphviz`语言支持子图，子图中的`N`和`E`在不同的代码块中，因此，`Graphviz`的`Graph`可以细分成三种：

1. **`G - graph/root graph`（图）：** 以`digraph`定义的是`有向图`，以`graph`定义的是`无向图`
1. **`S - subgraphs`（子图）：** 以代码块形式的即是子图（包括不以`cluster`开头定义的`subgraph`代码块），`S`内定义的`N`和`E`公共属性值是代码块内属性值，即超出`S`外，该公共属性值不起作用，`Subgraph`本身没有可显示的属性，`Subgraph`的作用
1. **`C - cluster subgraphs`（聚集子图）：** 以`cluster`开头定义的`subgraph`代码块，`C`不仅有`S`的容纳代码块内属性值的作用外，在其内定义的`N`还会被渲染在一个子图框内

**子图的类型（有向图还是无向图）与父图相同，子图的名称以`cluster`开头才被当成聚集子图渲染**。

```{.line-numbers}
digraph graph_name{
    bgcolor="transparent";//背景透明
    
    subgraph cluster_subgraph_name{//聚集子图
        node[shape=box];
        cluster_A -> cluster_B;
    }
    
    subgraph subgraph_name{//子图
        node[shape=none];
        sub_A -> sub_B;
    }
    
    {//匿名子图
        node[shape=octagon];
        nest_A -> nest_B;
    }
    
    global_A -> global_B;
    
    cluster_B -> global_B;
    sub_B -> global_B;
    nest_B -> global_B;
}
```

<!-- @import "./00_Diagram/02_gv_ex_subgraph.gv" -->


## Grpahviz节点实体之基本节点
`Graphviz`基本节点就是`节点标识符`加上`属性限定符“[]”`包围着的可选`节点属性`，如果基本节点不定义`label`属性值则将`节点标识符`作为文本进行显示。

```{.line-numbers}
digraph gv_ex_basic_node{
    bgcolor="transparent";//背景透明
    
    node_display_this_id;
    
    node_display_label[label="显示\n标签值"];
}
```
<!-- @import "./00_Diagram/03_gv_ex_basic_node.gv"-->

## Grpahviz节点实体之HTML节点

Graphviz的`label`属性支持`HTML`语法，使用`HTML`语法时：
1. `node`的`shape`属性设置成`none`
1. `node`的`margin`属性设置成`0`
1. `node`的`label`属性字符串通过尖括号`<...>`包含`HTML语法字符串`定义

**注：HTML节点的`label`属性值不建议使用引号包含，因为虽然可以被正确渲染，但脚本源码会失去`语法高亮`的效果**

```{.line-numbers}
digraph HTML_label_Example
{
    
    bgcolor="transparent";//背景透明
    
    html_ex_node[shape=none, margin=0, label=<
        <table border="0" cellborder="1" cellspacing="0" cellpadding="4">
            <tr>
                <td rowspan="2">test</td>
                <td>a</td>                
                <td rowspan="2">HTML-Like<br/>label</td>
            </tr>
            <tr>
                <td>b</td>
            </tr>
        </table>
    >];
}
```

<!-- @import "./00_Diagram/04_gv_ex_html_node.gv" -->

## Graphviz节点实体之Record节点

当Node的定义为`shape = record`时，label属性定义了Node的结构，具体为：

+ **`...|...`：** 分隔的字符串会在绘制的节点表现为一条分隔符
+ **`<...>`：** 定义了锚点（Anchor），锚点可以被引用为edge的起点或终点，引用语法是`${node_id}:${anchor_id}`，即用`:`引用node的anchor
+ **`{...}`：** 花括号里定义的分隔符分隔节点将从切换方向排布（原水平分隔将变成垂直分隔，原垂直分隔将变成水平分隔）

## Graphviz的连线实体

`digraph有向图`使用`->`定义`Edge`，`graph无向图`使用`-`定义`Edge`。
