

# Mermaid
## Mermaid简介
Mermaid（美人鱼）是开源的基于JavaScript的图表生成工具，通过伪代码（官方称“类Markdwon”）的语法书写源代码，经过Mermaid渲染生成图表。

Mermaid提供Flowchart（流程图）、Sequence Diagram(UML顺序图)、Gantt Diagram（甘特图）、Git版本图共4类图表。

Mermaid可以在Web程序、NodeJS程序以及其他支持Mermaid插件的程序中使用。

更方便的是，Mermaid可以在Markdown中以代码块的形式嵌入，包括ATOM、Gitbook等支持Markdown工具都可以方便地使用Mermaid。

比较完整的支持Mermaid的程序和软件可以查看Mermaid的官方文档。

Mermaid的相关链接：
+ 官方主页：https://github.com/knsv/mermaid
+ 官方文档：https://github.com/mermaidjs/mermaid-gitbook

## Mermaid语法
1. **显式声明图表类型：** 图表的开头必须显式地声明图表类型
    + **`graph`：** 流程图
    + **`sequenceDiagram`：** UML顺序图
    + **`gantt`：** 甘特图
    + **`gitGraph`：** Git版本图
1. **隐式定义节点标识符：** 标识符可以先定义再使用，也可以直接在使用而不必先定义，即第一次使用即是定义
1. **节点标识符覆盖：** 如果两个不同的图表元素（文本或形状不同）使用了相同的标识符，那么后一个图表元素将覆盖前一个图表元素所应出现的所有位置，从而导致前一个图表元素不会出现在图表中
1. **合法的节点标识符：**
1. **可选的空格符：** 节点标识符、节点定义保留符、连线定义保留符之间的空格以及文本前后的空格（包括双引号内的前后空格）将在绘制中被忽略
1. **可选语句定界符：** **`;`** 是可选的语句定界符
1. **注释：** `%%`两个百分号进行单行注释

### Flowchart
#### 图表方向
Flowchart的图表声明语句中需要包含图表的方向，图表的方向有4种：

1. TB(Top Bottom)/TD(Top Down)：自上向下
1. BT(Bottom Top)：自下向上
1. LR(Left Right)：自左向右
1. RL(Right Left)：自右向左

一般常用的是TB/TD和LR这两种。

#### 节点

1. 默认节点/直角矩形节点：**`id`** ，默认节点的id直接当成文本显示
1. 直角矩形节点（带文本）：**`id[text]`** ，id不当文本显示
1. 圆角矩形节点（带文本）：**`id(text)`** ，id不当文本显示
1. 圆形节点（带文本）：**`id((text))`** ，id不当文本显示
1. 非对称形状节点（带文本）：**`id>text]`** ，id不当文本显示
1. 菱形节点（带文本）：**`id{text}`** ，id不当文本显示

```{.line-numbers}
graph LR
    id1;
    id2[文本];
    id3(文本);
    id4((文本));
    id5>文本];
    id6{文本};
    中文标识符[文本];

    id1 --> id2;
    id2 --> id3;
    id3 --> id4;
    id4 --> id5;
    id4 --> id6;
    id6 --> 中文标识符;
```

#### 连线
Mermaid连线的线段分为3种：
1. 细实线：
1. 粗实线：
1. 虚线：

1. 单方向连接： **`id1 --> id2`**
1. 无方向连接： **`id1 --- id2`**
1. 单方向连接（带文本）： **`id1 --text--> id2`** 或 **`id1 -->|text| id2`**
1. 无方向连接（带文本）： **`id2 --text--- id2`** 或 **`id1 ---|text| id2`**
1. 虚线单方向连接：`id1 .-> id2`或`id1 -.-> id2`
1. 虚线无方向连接：`id .- id2`或`id1 -.- id2`或`id1 ..- id2`
1. 虚线单方向连接（带文本）：`id1 -.text.-> id2`
1. 虚线无方向连接（带文本）：`id1 -.text.- id2`
1. 粗线单方向连接：`id1 ==> id2`
1. 粗线无方向连接：`id1 === id2`
1. 粗线单方向连接（带文本）：`id1 ==text==> id2`或`id1 ==>|text| id2`
1. 粗线无方向连接（带文本）：`id1 ==text=== id2`或`id1 ===|text| id2`

```mermaid
graph TD;
    id1 ==>|text| id2;
    id3 .- id4
    A["A double quote:#quot;"] -->B["A dec char:#9829;"]

```

#### 保留字符转义、换码符、HTML标签和Fontawesome
流程图中文本如果包含保留字符，可以通过将文本包含在双引号内以避免补解释成保留字符。

流程图的保留字符包括：**`[`,`]`,`(`,`)`,`>`,`{`,`}`,`.`,`-`,`=`,`|`,`;`,`#`** 。

但HTML的标签字符`<`需要通过反斜杠进行转义，即如果需要输出`<`，在代码中需要书写为"\\<"

在双引号内，也可以输出换码符(`:char_number;`)。

文本中可以使用HTML标签（如<br/>、<b>等）。

流程图中的文本可以输出Fontawesome，语法是：`fa:icon_class_name`。

#### 子图
子图包含在`subgraph title`开头和`end`结尾的代码区域中。

子图绘制的注意点：
1. 节点绘制在 **首次使用的子图代码区域** 所在子图中，因此，即使全局代码区域中比子图代码区域先使用，也会绘制在子图中
1. 跨子图的连线虽然也可以在 **定义节点的子图代码区域** 中定义，但基于可读性的考虑，建议在全局代码区域中定义
1. 过多的子图绘制可能跟设想的排版效果相差较大，特别是绘制较多的跨子图连线时

```{.line-numbers}
graph TB
    c1-->a2

    subgraph one
        a1-->a2
    end

    subgraph two
        b1-->b2
    end

    subgraph three
        c1-->c2

    end

```

```mermaid
graph TD
c1-->a2
a1
a2

subgraph one
    a1-->a2
end

a1--"跨子区域<br/><b>跨行</b>连线"-->e1

subgraph two
    b1-->b2
end

subgraph three
    c1-->c2

end

subgraph four
    d1-->d2
end

e1-->d2

```

#### 样式


## Mermaid缺点
1. 流程图缺少“流程控制（判断、循环）”的原生语法
1. 流程图缺少“并发”的语法和图表元素，无法绘制包含“并发”的流程图
1. 流程图缺少“备注”、“图表说明”的语法和图表元素
1. 语法比较混乱，如输出Mermaid保留字符和HTML保留字符是两种不同的不相兼容的规则

```echarts
"series": [
{
  "name": "访问来源",
  "type": "pie",
  "radius": "55%",
  "data": [
    {
      "value": 235,
      "name": "视频广告"
    },
    {
      "value": 274,
      "name": "联盟广告"
    },
    {
      "value": 310,
      "name": "邮件营销"
    },
    {
      "value": 335,
      "name": "直接访问"
    },
    {
      "value": 400,
      "name": "搜索引擎"
    }
  ]
}
]
```
