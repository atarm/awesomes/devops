# PlantUML
<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=5 orderedList=true} -->
<!-- code_chunk_output -->

1. [Specification](#specification)
2. [UML Diagrams](#uml-diagrams)
    1. [Sequence Diagram时序图](#sequence-diagram时序图)
        1. [Participant参与者](#participant参与者)
        2. [Arrow箭头](#arrow箭头)
            1. [Basic Arrow基本箭头类型](#basic-arrow基本箭头类型)
            2. [Decorate Arrow修饰箭头](#decorate-arrow修饰箭头)
            3. [Arrow Direction箭头方向](#arrow-direction箭头方向)
            4. [Arrow Color箭头颜色](#arrow-color箭头颜色)
            5. [Message Autonumber消息自动编号](#message-autonumber消息自动编号)
3. [Non-UML Diagrams](#non-uml-diagrams)
    1. [Archimate diagram](#archimate-diagram)
    2. [Specification and Description Language(SDL)](#specification-and-description-languagesdl)

<!-- /code_chunk_output -->

## Specification

1. **代码区域：** 每张图表的代码区域通过"@startuml"开始、"@enduml"结束
1. **隐式图形类型：** 每张图表不必显示声明具体图表类型
1. **注释：** 支持单行注释和多行注释
    + 单行注释：**'**
    + 多行注释：**/'** 和 **'/**
1. **转义字符：** 跟绝大多数代码语言一样，对保留字符转义使用反斜杠“\\”
    + **“\\n”** 经常被用于文本换行
1. **图表元素与标识符：**
    + **标识符别名：** 一般默认使用图形元素的文本作为标识符，可以使用“as”关键字另起一个图形元素的 **alias（别名）**。
        + **注：一旦建立了别名后，原文本作为的标识符将释放，也就是说，这时如果再试图使用原文本作为标识符使用时，将被视为两个不同的图形元素**
    + **隐式声明：** 图表元素可以不必显式声明

{% uml %}
```puml
@startuml
'这是单行注释
/'  这是多行注释，第一行注释
    第二行注释
'/
@enduml
```
{% enduml %}

```PlantUML
@startuml
actor Bob #red
'The only difference between actor
'and participant is the drawing
participant Alice
'participant "I have a really \n long name" as L #99FF99
/' You can also declare:
participant L as "I have a really\nlong name" #99FF99 '/
Alice->Bob: Authentication Request
Bob->Alice: Authentication Response
'Bob->L: Log transaction
Bob->"I have a really \n long name": Another
Bob->"I have a really \n long name": Another
@enduml
```

## UML Diagrams

### Sequence Diagram时序图

#### Participant参与者

PlantUML提供6种参与者，关键词分别为：

1. **`participant`**
1. **`actor`**
1. **`boundary`**
1. **`entity`**
1. **`database`**

参与者在图表中的顺序可以通过关键词 **`order`** 进行自定义，数值越大，对应的参与者排在越右边

{% uml %}
```plantuml{align=center filename="plantuml_participants_demo.png"}
@startuml
participant Participant as p order 10
actor Actor as a order 9
boundary Boundary as b order 8
entity Entity as e order 6
database Database as d order 4
@enduml
```
{% enduml %}

```{.line-numbers}
@startuml
participant Participant as p order 10
actor Actor as a order 9
boundary Boundary as b order 8
entity Entity as e order 6
database Database as d order 4
@enduml
```

#### Arrow箭头

##### Basic Arrow基本箭头类型
箭头线段类型和箭头头部类型组合成基本箭头类型，总共可以组合成$2×6=12$种基本箭头类型。

1. 箭头线段类型
    1. **`-`** ：一条横杠绘制实线
    1. **`--`** ：两条横杠绘制虚线
1. 箭头头部类型
    1. **`>`** ：绘制粗箭头
    1. **`>>`** ：绘制细箭头
    1. **`\`** ：绘制单边粗箭头
    1. **`\\`** ：绘制单边细箭头
    1. **`/`** ：绘制单边粗箭头
    1. **`//`** ：绘制单边细箭头

**注意：** 单边箭头绘制上部还是下部跟箭头方向、斜杠字符都有关，具体请见 [箭头方向](#arrow-direction箭头方向) 部分示例。

{% uml %}
```plantuml{align=center filename="plantuml_basic_arrow_demo.png"}
@startuml
actor Actor as A
participant Participant as A
A -> P
A --> P
A ->> P
A -->> P
A -\ P
A --\ P
A -\\ P
A --\\ P
A -/ P
A --/ P
A -// P
A --//P
@enduml

```
{% enduml %}

##### Decorate Arrow修饰箭头

<!-- TODO -->

1. **`x`**：表示 **丢失** 的消息，使用 **`x`** 修饰箭头后，原来的箭头头部类型将 **失效**
1. **`o`**：使用 **`o`** 修饰箭头后，原来的箭头头部类型仍然 **有效**
1. **`[`** 或 **`]`** ：表示外部消息

```{.line-numbers}
actor Actor as A
participant Participant as A
A ->x P
A -\x P
A ->o P
A -\o P

@enduml
```

{% uml %}
```plantuml{align=center}
@startuml
actor Actor as A
participant Participant as A
A ->x P
A -\x P
A ->o P
A -\o P
A ->]
[-> A
@enduml

```
{% enduml %}


##### Arrow Direction箭头方向
可以有反向箭头或双向箭头。

```{.line-numbers}
@startuml
actor Actor as A
participant Participant as A
A -> P
A <- P
A <-> P
A \-P
@enduml
```

{% uml %}
```plantuml
@startuml
actor Actor as A
participant Participant as A
A -> P
A <- P
A <-> P
A -\ P
A \- P
A -/ P
A /- P
@enduml

```
{% enduml %}

##### Arrow Color箭头颜色

{% uml %}
```plantuml
@startuml
actor Actor as A
participant Participant as A
A -[#blue]> P
A <[#gray]- P
A <-[#lime]> P
A --[#blue]> P
@enduml

```
{% enduml %}

##### Message Autonumber消息自动编号

**`autonumber ${start} ${increment} ${number-style}`** ，一张图表可以有多个自动编号区域

## Non-UML Diagrams

### Archimate diagram


### Specification and Description Language(SDL)
SDL(Behavioral Design)作为图形标记补充到UML模型的Activity图，一共有6种图形，：

{% uml %}
```puml

@startuml

start
:State (UML Activity);
if (Decision Conditions?(UML Activity)) then (Yes)
    :(SDL)Procedure|
    :Input and other transtion triggering & guard conditions<
    :Output>
    :this/
    :Action]
    :Others}
else (No)
endif
stop

@enduml

```
{% enduml %}
