# Twitter Coding Styles

1. [Java Style Guide.](https://github.com/twitter-archive/commons/blob/master/src/java/com/twitter/common/styleguide.md)
1. [Effective Scala.](https://twitter.github.io/effectivescala/)
