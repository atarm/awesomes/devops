# XOps

## GitOps

```math
GitOps = XaC + MR|PR + CICD
```

### 为什么一定是`GitOps`❓

1. 版本管理
1. 分支策略
1. 代码审查
1. 历史记录
1. 用户体验

### Pipeline Design

- Source code version control源码版本控制
- Optimum branching strategy最优化分支策略
- Static analysis静态分析
- \> 80% code coverage大于80%的代码覆盖率
- Vulnerability scan
- Open source scan开源代码扫描
- Artifact version control制品版本控制
- Auto provisioning
- Immutable servers
- Integration testing
- Performance testing
- Build deploy testing automated for every commit
- Automated rollback
- Automated change order
- Zero downtime release
- Feature toggle

>[Tapabrata Pal. Focusing on the DevOps Pipeline.](https://medium.com/capital-one-tech/focusing-on-the-devops-pipeline-topo-pal-833d15edf0bd)
