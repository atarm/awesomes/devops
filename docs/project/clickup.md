# ClickUp

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [5W3H](#5w3h)
2. [Integrate with `DevOps`](#integrate-with-devops)

<!-- /code_chunk_output -->

## 5W3H

## Integrate with `DevOps`

1. [clickup. DevOps vs Agile: Ultimate Guide (2022).](https://clickup.com/blog/devops-vs-agile/)
