# Version

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Release Tags](#release-tags)
2. [Function Tags](#function-tags)
3. [License Tags](#license-tags)
4. [Futhermore](#futhermore)
    1. [Books and Monographs](#books-and-monographs)
    2. [Courses and Tutorials](#courses-and-tutorials)
    3. [Documents and Supports](#documents-and-supports)
    4. [Manuals and CheatSheets](#manuals-and-cheatsheets)
    5. [Papers and Articles](#papers-and-articles)
    6. [Playgrounds and Exercises](#playgrounds-and-exercises)
    7. [Examples and Templates](#examples-and-templates)
    8. [Standards and Specifications](#standards-and-specifications)
    9. [Softwares and Tools](#softwares-and-tools)
    10. [Miscellaneous](#miscellaneous)

<!-- /code_chunk_output -->

## Release Tags

1. `GA(General Availability)`: should be very stable and feature complete
    1. aka `Release`/`-RELEASE`
    1. aka `Stable`
    1. aka `Final`
1. `RC(Release candidate)`: probably feature complete and should be pretty stable - problems should be relatively rare and minor, but worth reporting to try to get them fixed for release
1. `M(Milestone)`: probably not feature complete; should be vaguely stable (i.e. it's more than just a nightly snapshot) but may still have problems
    1. aka `Pre`/`-PRE`
1. `SNAPSHOT`: 
1. `SR(Service Release)`: subsequent maintenance releases that come after major `-RELEASE`
1. `alpha`
1. `beta`

>1. [wiki. Software release life cycle.](https://en.wikipedia.org/wiki/Software_release_life_cycle)

## Function Tags

## License Tags

## Futhermore

### Books and Monographs

<!-- {contents here} -->

### Courses and Tutorials

<!-- {contents here} -->

### Documents and Supports

<!-- {contents here} -->

1. [wiki. Software versioning.](https://en.wikipedia.org/wiki/Software_versioning)

### Manuals and CheatSheets

<!-- {contents here} -->

### Papers and Articles

<!-- {contents here} -->

### Playgrounds and Exercises

<!-- {contents here} -->

### Examples and Templates

<!-- {contents here} -->

### Standards and Specifications

<!-- {contents here} -->

1. 👑 [Semantic Versioning.](https://semver.org/)

### Softwares and Tools

<!-- {contents here} -->

### Miscellaneous

<!-- {contents here} -->

<!--

授权和功能划分：
Trial：试用版，通常都有时间限制，有些试用版软件还在功能上做了一定的限制。可注册或购买成为正式版
Unregistered：未注册版，通常没有时间限制，在功能上相对于正式版做了一定的限制。可注册或购买成为正式版。
Demo：演示版，仅仅集成了正式版中的几个功能，不能升级成正式版。
Lite：精简版。
Full　version：完整版，属于正式版。

其他版本
Enhance　：增强版或者加强版　属于正式版1
Free　：自由版
Release　：发行版　有时间限制
Upgrade　：升级版
Retail　　：零售版
Cardware　：属共享软件的一种，只要给作者回复一封电邮或明信片即可。（有的作者并由此提供注册码等），目前这种形式已不多见。/　S
Plus　：属增强版，不过这种大部分是在程序界面及多媒体功能上增强。
Preview　：预览版
Corporation　&　Enterprise　：企业版
Standard　：标准版
Mini　：迷你版也叫精简版只有最基本的功能
Premium　：　贵价版
Professional(Pro)　：　专业版
Express　：　特别版
Deluxe　：　豪华版
Regged　：　已注册版

Build:内部标号
Delux:豪华版 (deluxe: 豪华的，华丽的)
DEMO演示版，一般会有功能限制
Full:完全版
Plus:加强版
Trial:试用版（一般有时间或者功能限制）

作者：虾米咬小米
链接：https://www.jianshu.com/p/fcadf3e3d875
来源：简书
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
-->
