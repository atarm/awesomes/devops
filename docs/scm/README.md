# 软件配置管理Software Configuration Management

## W&H

### What is SCM

### SCM VS VCS

`VCS`是现代`SCM`的基础和核心。

## Futhermore

### Books and Monographs

<!-- {contents here} -->

### Courses and Tutorials

<!-- {contents here} -->

### Documents and Supports

<!-- {contents here} -->

1. [wiki. Software configuration management.](https://en.wikipedia.org/wiki/Software_configuration_management)

### Manuals and CheatSheets

<!-- {contents here} -->

### Papers and Articles

<!-- {contents here} -->

1. [配置管理.](https://xueqing.github.io/blog/software_architecture/software_engineering/25-configuration-management/)

### Playgrounds and Exercises

<!-- {contents here} -->

### Examples and Templates

<!-- {contents here} -->

### Standards and Specifications

<!-- {contents here} -->

### Softwares and Tools

<!-- {contents here} -->

### Miscellaneous

<!-- {contents here} -->
