# 版本控制系统Version Control System

## What is VCS

`modern vcs`的两大功能：**版本管理** 和 **多人协作**。

## SCM and VCS

## CVCS and DVCS

### 集中式版本控制系统Centralized Version Control System

1. `CVS`
2. `IBM ClearCase`
3. `Microsoft VSS(Visual SourceSafe)`
4. `SVN(SubVersioN)`

### 分布式版本控制系统Distributed Version Control System

1. `BitKeeper`
2. `Git`
3. `Mercurial`/`HG`
4. `Bazaar`
5. `Perforce`: commercial VCS that is used by some large enterprises.
6. `Darcs`
