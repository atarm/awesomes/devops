# Configuration

## Merge Fast-Forward

```bash {.line-numbers}
git config --global merge.ff no
git config --global merge.commit no

# git pull = git fetch + git merge, don't want create a commit when pull
git config --global pull.ff yes
```
