# Brief History of Git

Linus讽刺地嘲笑git这个名字（在英式英语俚语中表示"不愉快的人"）。

>The name "git" was given by Linus Torvalds when he wrote the very first version. He described the tool as "the stupid content tracker" and the name as (depending on your way):
>
>- random three-letter combination that is pronounceable, and not actually used by any common UNIX command. The fact that it is a mispronunciation of "get" may or may not be relevant.
>- "global information tracker": you're in a good mood, and it actually works for you. Angels sing, and a light suddenly fills the room.
>- stupid. contemptible and despicable. simple. Take your pick from the dictionary of slang.

1. 2002年以前，志愿者们使用patch和tarball来协作开发Linux内核，即将每个人的修改打成补丁，然后通过邮件发送给Linus Torvalds，由Linus Torvalds来决定是否接受以及合并代码到Linux内核中。
2. 2002年，Linux内核社区开始使用BitKeeper来管理Linux内核的开发，BitKeeper是由BitMover公司开发的一款商业的分布式版本控制系统，BitMover公司授权Linux内核社区免费使用BitKeeper来管理Linux内核的开发。
3. 2005年，开发`Samba`的`Andrew`试图破解BitKeeper的协议，BitMover公司收回了Linux内核社区免费使用BitKeeper的授权，Linux内核社区不得不寻找其他的版本控制系统来管理Linux内核的开发。
4. 2005年4月3日，Linus Torvalds在Linux内核邮件列表上发布了Git的源代码。
5. 2008年，GitHub上线，GitHub是一个基于Git的代码托管平台，GitHub的出现使得Git得到了更加广泛的应用，成为了软件开发领域的最主流、最广泛使用的版本控制系统。

`Linus`一直痛恨使用集中式版本控制系统，他认为集中式版本控制系统的最大问题是必须联网才能工作，如果在某个时刻断网了，那么就没法提交代码，也没法和其他人协作开发。

分布式版本控制系统通常有一台充当"中央服务器"的主机，但这个服务器的作用仅仅是用来方便"交换"大家的修改，没有它大家也一样可以协作开发，只是交换修改不方便。

老一点的`Debian`或`Ubuntu`，有个软件也叫`GIT(GNU Interactive Tools)`，因此，`Git`只能叫`git-core`，后来，由于`Git`的名气实在太大，后来将`GNU Interactive Tools`改成`gnuit`，`git-core`改成`git`。
