# Git

## 分布式架构

## 逻辑结构

### 文件状态

1. modified
2. staged
3. committed

### 核心区域

1. 工作区
2. 暂存区
3. 本地仓库
4. 远程仓库

## 物理结构

1. `.git`
    1. HEAD
    2. refs
    3. index
    4. objects
    5. logs
    6. ...
2. `.`

## 分支模型

### create

### merge

1. `git merge`
2. `git rebase`
