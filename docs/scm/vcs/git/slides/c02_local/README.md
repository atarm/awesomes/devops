---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# Git Basic

<!-- slide -->
# Main Agendas

1. Git Logical and Physical Components
1. Initialize Repository

<!-- slide -->
# Git Use Recommend

<!-- slide -->
# `git init`

<!-- slide -->
# `git clone`

<!-- slide -->
# `git commit`

```bash{.line-numbers}
git commit -m"message"
git commit -am"message"
git commit --amend
```

<!-- slide -->
# `git status` - show track status

<!-- slide -->
# `git diff` - show changes of content

```bash{.line-numbers}
git diff ==> diff between workspace and stage(HEAD+1)
git diff <commit> ==> diff between workspace and <commit>
git diff --cache ==> diff between stage and HEAD
git diff --cache <commit> ==> diff between stage and <commit>
```

<!-- slide -->
# `git log` - Show Commit Histories

1. **`--petty=oneline`:**
1. **`--oneline`:** shorthand for "`--pretty=oneline --abbrev-commit`"
1. **`--graph`:**

<!-- slide -->
# `git reflog` - Show Command Histories

1. `reflog` is stand for `reference log`

<!-- slide -->
# `gitk` --> The git repository browser

<!-- slide -->
# `git-gui` - 

<!-- slide -->
# `git checkout`

1. `git checkout -- <file>`:  change file back to last `git add` or `git commit` status
1. `git checkout <branch>`: switch to a existed branch
1. `git checkout -b <branch>`: create and switch to a new branch, it equals to `git branch <branch>` and then `git checkout <branch>`

<!-- slide -->
# `git reset`

The git reset command has two main functions:

1. It manipulates the HEAD pointer. The HEAD pointer points to the current commit that you are working on. When you use the git reset command, you can move the HEAD pointer to a different commit.
2. It affects the staging area and the working area. The staging area is a temporary area where you can store changes that you want to commit. The working area is the area where you make changes to your code. When you use the git reset command, you can choose to either keep or discard the changes in the staging area and the working area.

---

1. `git reset --hard`: reset `stage` and `workspace` to a `repository` status
2. `git reset --soft`: reset `stage` to a `repository` status
3. `git reset --mixed`

<!-- slide -->
# `git switch` --> New Command to `git checkout <branch>`

<!-- slide -->
# `git branch`

1. `git branch`: show branches
1. `git branch <branch>`: create a branch
1. `git branch -d <branch>`: delete a branch

<!-- slide -->
# `git add`

<!-- slide -->
# `git rm`

<!-- slide -->
# `git mv`

<!-- slide -->
# `git clean`

1. 

<!-- slide -->
# `git cherry-pick`

<!-- slide -->
# `git rebase`

1. `rebase` scenarios：
    1. combine multi-commit：

<!-- slide -->
# Git Branch

<!-- slide -->
# `git branch`

```markdown{.line-numbers}
git branch
git branch -a
```

<!-- slide -->
# `git switch`

<!-- slide -->
# `git checkout`

<!-- slide -->
# `git merge`

```markdown{.line-numbers}
git merge <branch>
git merge --squash <branch>
```

<!-- slide -->
# Branch Limits

1. If branch b exists, no branch named b/anything can be created.

<!-- slide -->
# Fast Forward

1. **`git config merge.ff <true|false>`**

<!-- slide -->
# Git Stash

<!-- slide -->
# `git stash`

1. `git stash`
1. `git stash apply`
1. `git stash pop`
1. `git stash drop`
1. `git stash list`

<!-- slide -->
# Git Tag

<!-- slide -->
# `git tag`

<!-- slide -->
# `git show`
