---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Introduction to Git_"
footer: "@aRoming"
math: katex
---

<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Introduction to Git

>1. [廖雪峰. Git教程.](https://www.liaoxuefeng.com/wiki/896043488029600/896067008724000)
>2. [QuickRef.ME. Git.](https://quickref.me/git)

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [VCS: What and Why](#vcs-what-and-why)
2. [Git: What and Why](#git-what-and-why)
3. [Git: How](#git-how)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## VCS: What and Why

---

![height:500](./.assets/image/lots-of-docs.jpg)

>[廖雪峰. Git教程--Git简介.](https://www.liaoxuefeng.com/wiki/896043488029600/896067008724000)

---

1. 哪个版本是最新的？
2. 每个版本差异哪些？
3. 怎样合并多人修改？

---

### CVCS vs DVCS

![height:400](./.assets/image/cvcs.jpeg)
![height:400](./.assets/image/dvcs.jpeg)

---

### Main CVCS(Centralized VCS)

1. [`CVS(Concurrent Versions System)`](https://en.wikipedia.org/wiki/Concurrent_Versions_System)： `FOSS`(`GPL-1.0-or-later`), November 19, 1990
1. [`IBM Rational ClearCase`](https://en.wikipedia.org/wiki/Rational_ClearCase): proprietary && commercial, 1992
1. [`Microsoft VSS(Visual SourceSafe)`](https://en.wikipedia.org/wiki/Microsoft_Visual_SourceSafe): proprietary && commercial, 1994
1. [`SVN(Subversion)`](https://en.wikipedia.org/wiki/Apache_Subversion)：`OSS`(`Apache-2.0`), October 17, 2000

---

### Main DVCS(Distributed VCS)

1. [BitKeeper](https://en.wikipedia.org/wiki/BitKeeper): proprietary && commercial, May 4, 2000, released as `OSS`(`Apache-2.0`) in 9 May 2016
2. [Mercurial](https://en.wikipedia.org/wiki/Mercurial): `FOSS`(`GPL-2.0-or-later`), 19 April 2005
3. [Bazaar](https://en.wikipedia.org/wiki/GNU_Bazaar): `FOSS`(`GPL-2.0-or-later`), 26 March 2005
4. [Git](https://en.wikipedia.org/wiki/Git): `FOSS`(`GPL-2.0-only`), 7 April 2005

---

1. [廖雪峰. Git教程 - 集中式vs分布式.](https://www.liaoxuefeng.com/wiki/896043488029600/896202780297248)
2. [Comparison of version-control software](https://en.wikipedia.org/wiki/Comparison_of_version-control_software)

---

## Git: What and Why

>git: an unpleasant or contemptible person

---

>[Git (/ɡɪt/)](https://en.wikipedia.org/wiki/Git) is a **_distributed version-control system_** for tracking changes in source code during software development. It is designed for coordinating work among programmers, but it can be used to track changes in any set of files. Its goals include speed, data integrity, and support for **_distributed, non-linear workflows_**.
>
>Git was created by **_Linus Torvalds_** in 2005 for development of the Linux kernel, with other kernel developers contributing to its initial development. Its current maintainer since 2005 is **_Junio Hamano_**.

---

>The Linux kernel is an open source software project of fairly large scope. For most of the lifetime of the Linux kernel maintenance ( **_1991 - 2002_** ), changes to the software were passed around as **_patches and archived files_**. In **_2002_**, the Linux kernel project began using a **_proprietary DVCS called BitKeeper_**.

---

>In **_2005_**, the relationship between the community that developed the Linux kernel and the commercial company that developed BitKeeper broke down, and the tool's free-of-charge status was revoked. This prompted the Linux development community (and in particular Linus Torvalds, the creator of Linux) to develop their own tool based on some of **_the lessons they learned while using BitKeeper_**.

>[A Short History of Git.](https://git-scm.com/book/en/v2/Getting-Started-A-Short-History-of-Git)

---

### [The Goals of Git](https://git-scm.com/book/en/v2/Getting-Started-A-Short-History-of-Git)

1. super speed
2. simple design && elegant design
3. fully distributed
4. non-linear development supported: strong support for **_thousands of parallel branches_**
5. large project supported: able to handle large projects like the `Linux kernel` efficiently (**_both develop speed and data size_**)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Git: How

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Git Installation

---

1. `Linux`
    1. `RPM-based Distribution`： `sudo dnf install git-all`
    1. `Debian-based Distribution`: `sudo apt install git-all`
1. `MacOS`: `brew install git`
1. `Windows`: download and install `git` or `GitHub Desktop`
1. `Android`: `Pocket Git`
1. `iOS`: `Working Copy`

GUI Clients: <https://git-scm.com/downloads/guis/>

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Minimum Configuration

---

```bash{.line-numbers}
# set user name and email, default is "--local"
git config [--system|--global|--local] user.name "{your_name}"
git config [--system|--global|--local] user.email "{email@example.com}"
```

1. `local`: **_current repository_**, `./.git/config`
2. `global`: all repositories in **_current user_**,  `~/.gitconfig`
3. `system`: all repositories in **_current os_**, `/etc/gitconfig`

```bash{.line-numbers}
# edit configuration with default editor, default is "--local"
git config -e [--local|--global|--system]
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Git Concepts

---

![height:600](./.assets/image/git-transport-v1.png)

---

#### Physical Components

1. **`local folder`:** storage of `workspace`
1. **`local ".git" folder`:** storage of `stash`, `index` and `local repository`
1. **`remote .git folder`:** storage of `repository` in remote(server or other computer)

---

#### Logical Components

1. **`workspace/working tree`（工作区）:** `.`
1. **`index/stage/cached`（缓存区）:** `./.git/index`
1. **`local repository`（本地仓库）:**
    1. **`HEAD`:** pointer point to current branch
    1. **`branches`:**
    1. **`tags`:**
1. **`stash`（暂存区）**
1. **`remote repository`（远程仓库）**

---

#### Supported Protocols

1. **`local path`**
1. **`file`**
1. **`https`**
1. **`ssh`**

---

#### Attentions Before Using Git

1. Git only track file but not folder (empty folder will be ignored)
1. Git only see difference of text file (to each character) not binary file
1. Git can know some of file `mv` but not all, unless use `git mv`

---

#### Dissatisfaction of Binary Files

1. VCS can only fully control text files, in other words, you can see differences of each commit
1. Git will incremental commit text files, binary files will be committed entire each time

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Git Command

---

#### Command Syntax

1. **`git <command> [options] [<subcommand>] [<arg> <values>]`**
1. **`-<short-arg> <values>|--<full-arg> <values>`**

---

![height:600](./.assets/image/git-transport-v1.png)

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Git Workflow

---

#### Single Repository Workflow

1. `git-flow`
2. `github-flow`
3. `gitlab-flow`

---

#### Multi Repository Workflow

1. `fork`
2. work in your repository
3. `pull request`/`merge request`

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
