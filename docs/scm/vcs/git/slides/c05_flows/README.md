---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# Git Flows

> 1. [Git 使用规范流程](http://www.ruanyifeng.com/blog/2015/08/git-use-process.html)

<!-- slide -->
# Git Flow vs GitHub Flow vs GitLab Flow

<!-- slide -->
# Keep Synchronized with Remote While Non-master Branch Developing

```bash{.line-numbers}
$ git fetch origin
$ git rebase -i origin/master
......squash commits
```

**OR**

```bash{.line-numbers}
$ git commit --fixup  
$ git rebase -i --autosquash
```

<!-- slide -->
# When Non-master Branch Finished

```bash{.line-numbers}
$ git fetch origin
$ git rebase -i origin/master
......squash commits
$ git push --force origin myfeature
```

<!-- slide -->
# `git-flow`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `fork`

