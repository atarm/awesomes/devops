# Git

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=3 orderedList=true} -->

<!-- code_chunk_output -->

1. [是什么What](#是什么what)
    1. [设计哲学Design Philosophies](#设计哲学design-philosophies)
    2. [最佳实践Best Practices](#最佳实践best-practices)
2. [为什么Why](#为什么why)
3. [怎样学How](#怎样学how)
    1. [核心术语](#核心术语)
    2. [核心关注点](#核心关注点)
    3. [使用方法](#使用方法)
4. [Key Questions](#key-questions)
5. [Futhermore](#futhermore)
    1. [Standards or Specifications](#standards-or-specifications)
    2. [Documents or Supports](#documents-or-supports)
    3. [Manuals or CheatSheets](#manuals-or-cheatsheets)
    4. [Books or Monographs](#books-or-monographs)
    5. [Courses or Tutorials](#courses-or-tutorials)
    6. [Papers or Articles](#papers-or-articles)
    7. [Playgrounds or Exercises](#playgrounds-or-exercises)
    8. [Examples or Templates](#examples-or-templates)
    9. [Auxiliaries or Tools](#auxiliaries-or-tools)
    10. [Miscellaneous](#miscellaneous)

<!-- /code_chunk_output -->

## 是什么What

>Git is a **_free and open source_** **_distributed version control system_** designed to handle everything from small to very large projects with speed and efficiency.
>
>Git is easy to learn and has a tiny footprint with lightning fast performance. It outclasses SCM tools like Subversion, CVS, Perforce, and ClearCase with features like **_cheap local branching_**, **_convenient staging areas_**, and **_multiple workflows_**.
>
>>[git official homepage](https://git-scm.com/)

### 设计哲学Design Philosophies

1. 分布式版本控制
2. 分支管理
3. 跟综修改而不是跟综文件

### 最佳实践Best Practices

1. `branch` early, and `branch` often
1. `test and merge` early, and `test and merge` often
2. always use `--no-ff`

## 为什么Why

1. 免费且开源free and open source：
1. 极速且易扩展super fast and scalable：
1. 低成本的分支与合并cheap branching/merging：社会化编程（social programming）的必要条件之一

Git is a distributed VCS, which means that each developer has a local copy of the code. This makes it easy to work on code offline and to collaborate with other developers.Git is also very flexible and can be used for a variety of projects, from small personal projects to large enterprise-level projects.

The market share of Git is likely to continue to grow in the future, as it is a powerful and flexible VCS that is well-suited for a variety of projects.

## 怎样学How

### 核心术语

1. 5 areas：
    1. `stash`
    1. `working directory`
    1. `stage`
    1. `local repository`
    1. `remote repository`
1. 2 pointers：
    1. `branch`: point to a `commit` which indicate a `branch` (from start to the pointer)
    2. `HEAD`: point to current `commit` of current `branch`

### 核心关注点

1. how to tracking history
1. how to working together

### 使用方法

1. 命令界面command line interface
1. 图形界面graphical user interface
1. 编辑器与IDE插件plugin of editor or IDE

## Key Questions

1. `stage`的价值是什么？（`stage`不记录`version`）
1. `git`相对于`svn`效率提升的功能有哪些？
1. 哪些功能是`git`提供的？哪些功能是`github`提供的？哪些功能是`git`没提供但是蕴含的？

<!--
1. https://github.com/git/git-scm.com/issues/1239
-->

## Futhermore

### Standards or Specifications

<!-- {standards, specifications, conventions goes here} -->

### Documents or Supports

<!-- {trusted or official documents goes here} -->

1. [atlassian.com. Getting Git right: Learn Git with tutorials, news, and tips[DB/OL].](https://www.atlassian.com/git)
2. [git official document collection.](https://git-scm.com/doc)
3. [GitHub Learning Lab.](https://lab.github.com/): include many techniques resources besides `Git`, `GitHub`, `Markdown`, `Node` and so on

### Manuals or CheatSheets

<!-- {manuals goes here} -->

1. [arslanbilal/git-cheat-sheet.](https://github.com/arslanbilal/git-cheat-sheet)

### Books or Monographs

<!-- {publishes goes here} -->

1. 🌟 [Pro Git: An Open Source Book](https://git-scm.com/book/en/v2)

### Courses or Tutorials

<!-- {systematic courses and tutorials goes here} -->

#### Courses or Tutorials for Starters

1. 👍 [阮一峰. 常用Git命令清单.](http://www.ruanyifeng.com/blog/2015/12/git-cheat-sheet.html)
2. 👍 [廖雪峰. Git教程.](https://www.liaoxuefeng.com/wiki/896043488029600)
3. 👍 [图解Git](http://marklodato.github.io/visual-git-guide/index-zh-cn.html)
4. [猴子都能懂的git入门](https://backlog.com/git-tutorial/cn/)
5. [git community book](http://shafiul.github.io/gitbook/index.html)
    1. [git community book中译本](http://gitbook.liuhui998.com/)
6. [Learn Git - Tutorials, Courses, and Books](https://gitconnected.com/learn/git)
7. [Atlassian-Bitbucket: Tutorials](https://www.atlassian.com/git/tutorials)
8. [Git Ready: learn git one commit at a time](http://gitready.com/)
9. [Learn Git Version Control using Interactive Browser-Based Scenarios](https://www.katacoda.com/courses/git)
10. [Git in The Trenches](http://cbx33.github.io/gitt/intro.html)

#### Courses and Tutorials for Advancers

1. 👍 [阮一峰. Git原理入门.](https://www.ruanyifeng.com/blog/2018/10/git-internals.html)

### Papers or Articles

<!-- {special topics goes here} -->

1. 👍 [atlassian. Advanced Tips.](https://www.atlassian.com/git/tutorials/advanced-overview)
2. [Flight Rules for Git](https://github.com/k88hudson/git-flight-rules)

#### Git Workflow

1. [Introduction to GitLab Flow (with Git Flow& GitHub Flow Compared)](https://docs.gitlab.com/ee/topics/gitlab_flow.html)
2. [GitLab Workflow: An Overview](https://about.gitlab.com/blog/2016/10/25/gitlab-workflow-an-overview/)
3. [Understanding the GitHub flow](https://guides.github.com/introduction/flow/)
4. [5 types of Git workflow that will help you deliver better code](https://buddy.works/blog/5-types-of-git-workflows)

### Playgrounds or Exercises

<!-- {exercises here} -->

#### Playgrounds for Starters

1. [GitHub: Resources to learn Git](http://try.github.io/)
    1. 🌟 [Git-it: a (Mac, Win, Linux) Desktop App for Learning Git and GitHub](https://github.com/jlord/git-it-electron)
    1. 👍 🌟 [Learn Git branching](https://learngitbranching.js.org/) （可以通过`sandbox`保存进度）
1. [Getting started with git](https://play.instruqt.com/public/topics/getting-started-with-git)

#### Playgrounds for Advancers

1. 🌟 [Git exercises](https://gitexercises.fracz.com)

### Examples or Templates

<!-- {examples here} -->

### Auxiliaries or Tools

<!-- {auxiliaries here} -->

1. [github/gitignore.](https://github.com/github/gitignore): A collection of useful `.gitignore` templates
1. [michaelliao/gitignore-online-generator](https://github.com/michaelliao/gitignore-online-generator): A useful `.gitignore` online generator.
1. [Visualizing Git Concepts with D3](http://onlywei.github.io/explain-git-with-d3/)
    1. [Visualizing Git](http://git-school.github.io/visualizing-git/)

### Miscellaneous

<!-- {misc here} -->
