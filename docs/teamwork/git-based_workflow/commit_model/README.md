# Commit Message

## Conventional Commits

前置条件：每次提交只完成一个任务。

## Futhermore

### Tutorials and Articles

1. [Conventional Commits](https://www.conventionalcommits.org/)
1. [pvdlg/conventional-changelog-metahub](https://github.com/pvdlg/conventional-changelog-metahub)
1. [Changelog from Conventional Commits - Github Action](https://github.com/marketplace/actions/changelog-from-conventional-commits)
1. [conventional-changelog/standard-version](https://github.com/conventional-changelog/standard-version)
1. [brianclements/Commit Formatting.md](https://gist.github.com/brianclements/841ea7bffdb01346392c)
1. [commitizen和cz-customizable配置git commit message](https://www.programminghunter.com/article/5222399418/)
1. [如何配置 Git Commit Message](https://zhuanlan.zhihu.com/p/69635847)
1. [Cz工具集使用介绍 - 规范Git提交说明](https://juejin.cn/post/6844903831893966856)
1. [实现自定义git提交规范](https://juejin.cn/post/6962056746328129567)

### Assistants and Tools

1. [Zhengqbbb/cz-git](https://github.com/Zhengqbbb/cz-git)
1. [streamich/git-cz](https://github.com/streamich/git-cz)
1. [commitizen/cz-cli](https://github.com/commitizen/cz-cli)
1. [leoforfree/cz-customizable](https://github.com/leoforfree/cz-customizable)
