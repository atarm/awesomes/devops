# Branch Model分支模型

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [分支模型是什么](#分支模型是什么)
2. [仓库内部分支模型](#仓库内部分支模型)
    1. [仓库内部分支模型的主要关注点](#仓库内部分支模型的主要关注点)
    2. [`git-flow` VS `github-flow` VS `gitlab-flow`](#git-flow-vs-github-flow-vs-gitlab-flow)
        1. [`git-flow`](#git-flow)
            1. [`git-flow`辅助工具](#git-flow辅助工具)
        2. [`github-flow`](#github-flow)
        3. [`gitlab-flow`](#gitlab-flow)
        4. [对比](#对比)
    3. [`one-flow`](#one-flow)
    4. [Etcetera](#etcetera)
3. [仓库间分支模型](#仓库间分支模型)
    1. [`forking`](#forking)
4. [Futhermore](#futhermore)
    1. [Tutorials and Articles](#tutorials-and-articles)
    2. [Manuals and CheatSheets](#manuals-and-cheatsheets)
    3. [Playgrounds and Exercises](#playgrounds-and-exercises)

<!-- /code_chunk_output -->


## 分支模型是什么

分支模型是研发工作流的核心。`git`最主要用于多人协作的软件开发，多人协作意味着必须有一个共同遵循的、（较）规范的工作流程，以便提高协作效率、降低沟通成本、减少出错机率，同时这个工作流程应当

<!--
## 基本管理策略

1. 主干开发、主干发布
1. 主干开发、分支发布
1. 分支开发、主干发布
-->

## 仓库内部分支模型

### 仓库内部分支模型的主要关注点

1. 分支类型
    1. 分支生存期：哪些是常设分支，哪些是临时分支（合并完后被删除）
    1. 分支稳定性：哪些是开发分支（易变分支），哪些是版本分支/发布分支（稳定分支）
1. 分支基准
    1. 分支创建：分支基准于哪个分支创建
    1. 分支合并：分支合并到哪些分支
1. 分支权限
    1. 共享分支：哪些分支是可共享给其他成员的（其他成员可读）
    1. 可写权限：哪些分支的写权限分配哪些成员，写权限包括`push`和`merge`

<!--
1. 分支权限
    1. 哪些分支是共享分支，哪些分支是个人分支
    1. 共享分支的权限分配策略是什么
1. 分支合并场景
    1. 合并分支在云端操作还是在本地操作


1. 多少个常设分支，多少个临时分支
    1. 常设分支中哪个是开发分支
    1. 常设分支中哪个是版本分支/发布分支
1. 哪些分支是共享的，哪些分支是个人的
    1. 哪些分支的哪些操作是云端操作，哪些分支的哪些操作是本地操作
        1. 合并操作是否必须云端操作
    1. 各个分支的权限分配策略是什么
1. 分支基于哪个分支，合并到哪个分支
-->

### `git-flow` VS `github-flow` VS `gitlab-flow`

#### `git-flow`

`git-flow`是`Vincent Driessen`在 **2010-01-05** 发表于他的`blog`上的一篇名为[A successful Git branching model](https://nvie.com/posts/a-successful-git-branching-model/)的文章中提出的`git-based`工作流模型。

**`git-flow`的图示参见["Vincent Driessen. A successful Git branching model. 2010-01-05."](https://nvie.com/posts/a-successful-git-branching-model/)和["Cameron McKenzie. A better Gitflow diagram with branches, merges and color. 2021-02-24."](https://www.theserverside.com/infographic/A-better-Gitflow-diagram-with-branches-merges-and-color)

<!--TODO:是基于"版本发布"场景吗？还是基于"重型持续集成"场景？-->

`git-flow`基于"`feature-branching strategy`"，`git-flow`的分支类型有：

1. 两个常设分支
    1. `main`：版本分支，稳定的发布分支
    1. `develop`：开发分支，易变的开发分支
1. 三类临时分支
    1. `feature`：功能分支，基于`develop`、合并到`develop`
    1. `hotfix`：补丁分支，基于`main`、合并到`main`和`develop`
    1. `release`：预发分支，基于`develop`、合并到`main`和`develop`

["Cameron McKenzie. A better Gitflow diagram with branches, merges and color. 2021-02-24."](https://www.theserverside.com/infographic/A-better-Gitflow-diagram-with-branches-merges-and-color)将`git-flow`增加了`release-branching strategy`，即，`main`的某个`release-tag`需要`patches`和`hotfixes`时，从该`release-tag`创建一个低频变更的`support`分支。

❗ ❗ ❗

1. `release`不是发布分支，是预发布分支（`pre-release`），对应的环境是`UAT`环境
1. `main`和`develop`两个常设分支相互间不进行直接同步（`merge`或`rebase`），他们之间的同步通过`hotfix`和`release`分支都`merge`到这两个分支进行简接的同步
1. 版本通过`main`分支上的`tag`发布
1. `merge`操作使用`--no-ff`

❗ ❗ ❗

>1. 👑 [Vincent Driessen. A successful Git branching model. 2010-01-05.](https://nvie.com/posts/a-successful-git-branching-model/)
>1. 👍 ["Cameron McKenzie. A better Gitflow diagram with branches, merges and color. 2021-02-24."](https://www.theserverside.com/infographic/A-better-Gitflow-diagram-with-branches-merges-and-color)
>1. [Git Flow开发模型](https://blog.xiaoquankong.ai/Git%20Flow%E5%BC%80%E5%8F%91%E6%A8%A1%E5%9E%8B/)

##### `git-flow`辅助工具

1. [nvie/gitflow](https://github.com/nvie/gitflow)：约2012-09-25停止更新
    1. [Using git-flow to automate your git branching workflow](https://jeffkreeftmeijer.com/git-flow/)
1. [petervanderdoes/git-flow (AVH Edition)](https://github.com/petervanderdoes/gitflow-avh)：`nvie/gitflow`的`fork`，约2019-05-23停止更新
    1. [前端综合能力系列之git与gitflow.](https://juejin.cn/post/6844903593699442702)

#### `github-flow`

>1. [github.com. GitHub flow.](https://docs.github.com/en/get-started/quickstart/github-flow)

`github-flow`基于"持续发布"场景，`github-flow`的分支类型有

1. 一个常设分支`main`：主分支，"持续发布"的发布分支和版本分支
1. N个临时分支：所有的临时分支基于`main`、合并到`main`，不要求显式区分临时分支的类型

#### `gitlab-flow`

`gitlab-flow`同时提供基于"版本发布"场景和基于"持续发布"场景的两套模型，`gitlab-flow`的基本策略是常设分支采用`upstream first policy`（上游优先），`upstream first`的典型案例如：`google chromium`和`redhat`。

`upstream first`要求只有`directly upstream`的变化才能引起当前分支的变化，依次类推到`directly downstream`，`directly upstream`的变化通过`cherry-pick`的方式应用到其他常设分支。

>1. [The Chromium Projects. Upstream First.](https://www.chromium.org/chromium-os/chromiumos-design-docs/upstream-first/)
>1. [RedHat. What is an open source upstream?](https://www.redhat.com/en/blog/what-open-source-upstream)

`gitlab-flow`的分支类型有：

1. N个常设分支：
    1. `main`：主分支，易变的开发分支
    1. "持续发布"场景，**有限个** `environment branches`：每个`environment`一个常设分支，并确定`main`（总是顶流）与各个`environment`之间的上下游关系
    1. "版本发布"场景，**无限个** `release branches`：每个`release version`一个常设分支，每个`release version`分支的`upstream`都是`main`，`release version`只`cherry-pick`来自于`main`的`bug patch`（即`release version branch`只更新`bug`）
1. 临时分支：建议所有的临时分支基于`main`、合并到`main`（也可基于其他临时分支、合并到该分支），不要求显式区分临时分支的类型（统一称为`feature`）

分为三种模型：`production branch`（基于"持续发布"场景）、`environment branches`（基于"持续发布"的"多环境"场景）、`release branches`（基于"版本发布"场景）

>1. [GitLab.com. Introduction to GitLab Flow.](https://docs.gitlab.com/ee/topics/gitlab_flow.html)

#### 对比

1. 三种分支模型的共同点
    1. `FDD(Feature-Driven Development)`：需求是开发的起点
    1. `feature branch`：从需求创建独立的临时分支，然后合并到约定的共享分支
1. `github-flow`和`gitlab-flow`的常设分支都是共享分支，合并到共享分支均通过`remote`的`merge/pull request -> code review -> merge`执行
1. `gitlab-flow`的`release branches`不再合并`new feature`、只合并`bug patch`，`git-flow`的`release branch`（`main`）是一个不断演进的共享分支
    1. `gitlab-flow`的`release`是一个分支
    1. `git-flow`的`release`是一个`main`上的`tag`

### `one-flow`

>1. Adam Ruka. series of articles on working with the Git source control system.
>    1. [Adam Ruka. GitFlow considered harmful.](https://www.endoflineblog.com/gitflow-considered-harmful)
>    1. [Adam Ruka. Follow-up to 'GitFlow considered harmful'.](https://www.endoflineblog.com/follow-up-to-gitflow-considered-harmful)
>    1. [Adam Ruka. OneFlow – a Git branching model and workflow.](https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow)
>    1. [Adam Ruka. Implementing OneFlow on GitHub, BitBucket and GitLab.](https://www.endoflineblog.com/implementing-oneflow-on-github-bitbucket-and-gitlab)

### Etcetera

1. [jbenet/simple-git-branching-model. a simple git branching model.](https://gist.github.com/jbenet/ee6c9ac48068889b0912)

## 仓库间分支模型

### `forking`

## Futhermore

### Tutorials and Articles

1. 👍 [Atlassian.com. Comparing Workflows.](https://www.atlassian.com/git/tutorials/comparing-workflows)
    1. [oldratlee. Comparing Workflows（中译版）.](https://github.com/oldratlee/translations/tree/master/git-workflows-and-tutorials)
1. 🌟 [阮一峰. Git使用规范流程.](https://www.ruanyifeng.com/blog/2015/08/git-use-process.html)
1. 🌟 [阮一峰. Git分支管理策略.](https://www.ruanyifeng.com/blog/2012/07/git.html)
1. 🌟 [阮一峰. Git工作流程.](https://www.ruanyifeng.com/blog/2015/12/git-workflow.html)
1. ⭐ [lucidchart.com. How to visualize your branching strategy.](https://www.lucidchart.com/blog/how-to-visualize-your-branching-strategy)

### Manuals and CheatSheets

1. [git-flow cheatsheet](https://danielkummer.github.io/git-flow-cheatsheet/)

### Playgrounds and Exercises

1. [umr-marbec/git-training](https://github.com/umr-marbec/git-training)
