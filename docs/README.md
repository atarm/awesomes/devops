# DevOps

## 5W1H

### 是什么What

`devops`并不仅仅是工具的集合，也不仅仅是组织架构的变革，更是企业文化和思想观念的变革。如果不能改变观念，即使将工具和人员放在一起，也不会产生火花。

#### 它是什么What It Is

>`DevOps` is a methodology that helps engineering teams build products by continuously getting user feedback.
>
>

>`devops` is a mindset, a culture, and a set of technical practices. It provides communication, integration, automation, and close cooperation among all the people needed to plan, develop, test, deploy, release, and maintain a solution.

##### Gartner Defined

>DevOps represents a change in IT culture, focusing on rapid IT service delivery through the adoption of agile, lean practices in the context of a system-oriented approach. DevOps emphasizes people (and culture), and it seeks to improve collaboration between operations and development teams. DevOps implementations utilize technology — especially automation tools that can leverage an increasingly programmable and dynamic infrastructure from a life cycle perspective.
>><https://www.gartner.com/en/information-technology/glossary/devops>

- 软件工程角度： 一组过程、方法与工具的总和，用于更好地持续整合用户反馈并构建产品
    - 工具：infrastructure for building and deploying code
- 企业流程角度： 一种文化、一套流程机制，用于促进开发、运维和质量保障部门之间的沟通、协作和整合

##### `CAMS` Model

1. Culture
1. Automation
1. Measurement
1. Sharing

>[DevOps Principles- The CAMS Model](https://medium.com/@seanguthrie/devops-principles-the-cams-model-9687591ca37a)

`devops`某种意义上，是从业者对一些核心关注点达到共识的一个术语。

##### `DevOps` VS `全面质量管理`

##### `waterfall` VS `agile` VS `devops`

![](./.assets/diagram/models_in_period.png)

##### 我们的定义Our Defined

`DevOps`不是全新的软件工程工程实践框架，而是整合已有的软件工程模式和软件工程实践于一体的工程实践框架，在文化上注重`sharing`和`cooperate`，在管理上注重`lean`和`measurement`，在过程上注重`iterate`和`access control`，在技术上注重`integrate`/`pipeline`和`automate`，`devops`的目标是减短交付、部署、发现和修复缺陷的时间，并提高软件质量。

从层次维度看，`devops`有三个层次：文化、管理实践、技术工具

`devops`文化强调：

1. `continuous持续`：快速、不断、无缝
1. `iterate迭代`：允许不完美，迭代改进
1. `automate自动化`：测试自动化、运维自动化、流程自动化，自动化也要求测试人员和运维人员也进行编程开发，而测试人员和运维人员编程开发的服务对象是开发人员，换句话说，开发人员开发的软件是`business software`，而测试人员和运维人员开发的软件是`CASE software`
1. `respect尊重`
1. `sharing分享`
1. `collaboration/cooperation协作`：无信息孤岛
1. `quality质量`
1. `efficiency效率`

管理实践大融合

1. `Agile`
1. `Lean`
    1. `value stream mapping`
    1. `kanban board`
    1. `lead time`
    1. `single pice flow`
1. `Lean Software Development`/`LeanIT`
1. `CICD`
1. `TPS(Toyota Production System)`/`Toyota Kata`
1. `ITSM`

技术工具大串联

1. `integrate`/`pipeline`
1. `automate`：使用软件开发（`dev`）技术，来自动化测试（`test`）和运维（`ops`）的工作

#### 它有什么What It Has

1. 需求/规划：
    1. 项目管理
    1. 管理看板
1. 开发：
    1. 代码托管
    1. 代码检查
    1. 编译构建
    1. 依赖关系
1. 测试：
    1. 测试管理
    1. 功能测试
    1. 接口测试
    1. 非功能测试
1. 发布：
    1. 部署
    1. 发布

### 为什么Why

#### 为什么有它The Problems

1. 测试环境OK，生产环境不OK
1. A服务器OK，B服务器不OK
1. 配置变化他们知道，我不知道
1. 大量人肉运维，全靠手速效率提升
1. 变更不可预期，被逼降低变更频次
1. 投产是一场决战到天亮的大戏
1. 身心俱疲，只想躺平

#### 为什么用它Its Strengths

### 什么时候在哪里When and Where

#### DevOps考古史When and From Where

1. 史前史：
    1. 2008，多伦多，敏捷大会：Patrick DeBois 和AndrewClay Shafer 先生首次提议讨论"敏捷基础架构"话题，接着Flickr的《每天部署10次》
    1. 2009，比利时，首届DevOps Days活动：Patrick DeBois 先生首次在公开场合提出"DevOps"这一名词
1. 定史：首届`devops days活动`后`#DevOpsDays`在`twitter`上被简写与`#DevOps`，此事件被广泛认为是`DevOps`的定史里程碑事件，此后，`DevOps`一词成为在各种活动中热议和讨论的焦点话题
1. 史后史：
    1. 2010，美国山景城(Mountain View)，DevOps Days 年会：
        1. Damon Edwards先生用一个缩写"CAMS"诠释了DevOps，即文化（Culture）、自动化（Automation）、测量（Measurement or Metrics）和分享（Sharing）
        1. Jez Humble先生将"L"精益 (Lean) 原则也加入其中，最终变成了CALMS

>1. 刘征, 2016. [关于DevOps的这些事(EB/OL)](http://mp.weixin.qq.com/s?__biz=MzIyMjQ2Mjc1NQ==&mid=100000458&idx=1&sn=2b9a246f930c080eb8ccce6cf55f57d0&chksm=682c54865f5bdd90e237285d23805ae04b1917408b90623e5dfb91cf5ebc3e19d596d8844750#rd). `[2016-11-21](2022-05-20)`. <http://mp.weixin.qq.com/s?__biz=MzIyMjQ2Mjc1NQ==&mid=100000458&idx=1&sn=2b9a246f930c080eb8ccce6cf55f57d0&chksm=682c54865f5bdd90e237285d23805ae04b1917408b90623e5dfb91cf5ebc3e19d596d8844750#rd>.

#### When and To Where

### 谁用它Who

### 怎样做How

#### `GQM` of `devops`

1. 怎样才能有效地打破原有的壁垒？
1. 怎样才能有效地改造现有的流程？
1. 怎样保证自动化情况下的安全审查？
1. 怎样才被认为是实施了`devops`（实施`devops`的`criteria`是什么）？

#### Actions

1. 从`VCS`开始
1. 从`CI/CD pipeline`开始
1. 从`container deploy`开始

## Futhermore

### Histories and Overviews

1. 刘征, 2016. [关于DevOps的这些事(EB/OL)](http://mp.weixin.qq.com/s?__biz=MzIyMjQ2Mjc1NQ==&mid=100000458&idx=1&sn=2b9a246f930c080eb8ccce6cf55f57d0&chksm=682c54865f5bdd90e237285d23805ae04b1917408b90623e5dfb91cf5ebc3e19d596d8844750#rd). `[2016-11-21](2022-05-20)`. <http://mp.weixin.qq.com/s?__biz=MzIyMjQ2Mjc1NQ==&mid=100000458&idx=1&sn=2b9a246f930c080eb8ccce6cf55f57d0&chksm=682c54865f5bdd90e237285d23805ae04b1917408b90623e5dfb91cf5ebc3e19d596d8844750#rd>.
1. XebiaLabs. [DevOps Tools Periodic Table](https://digital.ai/devops-tools-periodic-table).
1. 许峰. [一文收录16张DevOps "拍照神图"(DB/OL)](https://blog.51cto.com/u_15127518/2658838)

### Resources

1. 👍 [jhuangtw/xg2xg](https://github.com/jhuangtw/xg2xg): list tools and technologies of `google internal`, `google external`(`google open-sourced`) and `open source`

### 经典著作或文章

1. PHD N F, HUMBLE J, KIM G, 2018. Accelerate: The Science of Lean Software and DevOps: Building and Scaling High Performing Technology Organizations[M]. IT Revolution.
1. KIM G, 2019. The Unicorn Project: A Novel about Developers, Digital Disruption, and Thriving in the Age of Data[M]. IT Revolution.
1. KIM G, BEHR K, SPAFFORD G, 2018. The Phoenix Project: A Novel about IT, DevOps, and Helping Your Business Win[M]. IT Revolution.
1. KIM G, HUMBLE J, DEBOIS P, 等, 2021. The DevOps Handbook: How to Create World-Class Agility, Reliability, & Security in Technology Organizations[M]. IT Revolution.
1. KIM G, WILLIS J, 2018. Beyond The Phoenix Project: The Origins and Evolution Of DevOps [Official Transcript of The Audio Series](M). IT Revolution.
