# 持续集成/持续交付-持续部署

## `CICD`的前置条件

1. `continuous integration`的前置条件：`version control`和`automated building`(包含`automated testing`)
1. `continuous delivery`的前置条件：`continuous integration`和`artifacting`
1. `continuous deploy`的前置条件：`continuous delivery`和`virtualization`

### `automated testing`的关键节点

1. 触发事件
1. 测试脚本
    1. 运行测试用例
    1. 校验输出结果：实际输出与预期输出是否一致或在偏差允许范围内
    1. 判断测试结果：本测试用例或本测试用例集是否通过
    1. 记录测试结果及其上下文
