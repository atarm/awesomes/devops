---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# GitHub and GitLab

<!-- slide -->
# What is GitHub and GitLab from user side?

1. code hosting and publishing
1. extra features
    1. software engineering platform
    1. writing platform
    1. social platform

<!-- slide -->
# What is GitHub and GitLab from technical side?

1. a web which is git based
1. key features
    1. `code hosting and publish`
    1. `social programming`
        1. `Git`/`VCS`
        1. `pull request`/`merge request`: the most significant feature
        1. `issue`
1. addon features
    1. `wiki`
    1. `pages`
    1. `gist`
1. powerful integrations:
    1. API
    1. DevOps toolchain

<!-- slide -->
# Some special files

1. README.md
1. LICENSE.md
1. [CONTRIBUTING.md](https://github.blog/2012-09-17-contributing-guidelines/) 

<!-- slide -->
# GitLab

1. 基于`Git`搭建的web服务
1. 前端采用`Nginx`
1. 后端采用`Ruby on Rails`框架和`unicorn`
1. 数据库支持`MySQL`和`PostgreSQL`
1. `Redis`
1. 基于`MIT许可协议`

<!-- slide -->
# GitLab Member

1. Guest：可以创建issue、发表评论，不能读写版本库
1. Reporter：可以克隆代码，不能提交，可以赋予测试、产品经理此权限
1. Developer：可以克隆代码、开发、提交、push，可以赋予开发人员此权限
1. MainMaster：可以创建项目、添加tag、保护分支、添加项目成员、编辑项目，一般GitLab管理员或者CTO才有此权限

<!-- slide -->
# Enterprise

<!-- slide -->
# `fork` and `upstream`

> <https://stackoverflow.com/a/6286877>

<!-- slide -->
# Social Programming

1. [How we use Pull Requests to build GitHub](https://github.blog/2012-05-02-how-we-use-pull-requests-to-build-github/)
1. 

<!-- slide -->
# Upstream in GitHub/GitLab

1. if we `fork` `some-other/a-repo` to `myself/a-repo` and then `clone` to local, the `myself/a-repo` is automatic be `origin`, and usually, we would use `git remote add upstream git://github.com/some-other/a-repo` let `some-other/a-repo` be a **remote `upstream` branch**
1. if we modified somewhere and then use `git push -u origin master`, the `master` will associate and tracking `origin/master`

<!-- slide -->
# `hub`

1. `brew install hub`
