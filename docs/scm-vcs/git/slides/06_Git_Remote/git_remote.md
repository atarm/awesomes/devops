---
presentation:
  theme: white.css
  width: 1200
  height: 800
  slideNumber: true
  enableSpeakerNotes: true
  transition: zoom
  transitionSpeed: default
---

<!-- slide -->
# Git Remote

<!-- slide -->
# `git clone`

<!-- slide -->
# `git remote` --> Show Remotes

<!-- slide -->
# `git push` --> Push Local Repository  to Remote

<!-- slide -->
# `git fetch` --> Fetch Remote to Local Repository

<!-- slide -->
# `git pull` --> Pull Remote to Local Repository and Workspace

<!-- slide -->
# `git checkout`

1. `git checkout -b <local_branch> <remote_branch>`: create a local branch based on remote branch

<!-- slide -->
# `git branch`

1. `git branch [--no-track] <local_branch> <remote_repo>/<remote_branch>`
1. `git branch --set-upstream-to=<remote_branch> <local_branch>`
1. `git branch --set-upstream-to <branch-name> <remote_repo>/<remote_branch>`

<!-- slide -->
# Set Default Remote Name

```git{.line-numbers}
# ${repo_base}/.git/config
[branch "master"]
  remote = origin
  merge = refs/heads/master
```

<!-- slide -->
# upstream

## `upstream` means `local branch` tracking with a `remote branch`

```bash{.line-numbers}
# list branch with upstream information
git branch -vv

# set branch tracking with a remote branch, note: remote_branch must existed
git branch --set-upstream-to=<remote_branch> [local_branch]
or
git branch -u <remote_branch> [local_branch]

# unset upstream
git branch --unset-upstream [local_branch]

# push and set upstream
git push -u <remote_repo> [remote_branch_name]
```
