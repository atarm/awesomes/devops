---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# Git Stash

<!-- slide -->
# `git stash`

1. `git stash`
1. `git stash apply`
1. `git stash pop`
1. `git stash drop`
1. `git stash list`