# Snippet

## 从远程仓库拉取所有分支

```sh {.line-numbers}
git branch -r | grep -v '\->' | while read remote; do git branch --track "${remote#origin/}" "$remote"; done
git fetch --all
git pull --all
```
