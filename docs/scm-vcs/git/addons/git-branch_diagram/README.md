# Git Branch Diagram

## Futhermore

### Articles and Papers

1. 👍 [Bryan Braun. Drawing git branching diagrams.](https://www.bryanbraun.com/2020/04/24/drawing-git-branching-diagrams/)
1. [diagrams.net. How to create a gitflow diagram.](https://www.diagrams.net/blog/gitflow-diagram)
1. [gliffy.com. What is a Gitflow Diagram? How to Make a Gitflow Diagram & Visualize Branching Strategies.](https://www.gliffy.com/blog/gitflow-diagrams)
    1. [Gitflow Diagram Tutorial | Visualize Your Branching Strategy](https://www.youtube.com/watch?v=UCXS9ybIkkQ)
1. [EdrawMax. Gitflow Diagram.](https://www.edrawmax.com/article/gitflow-diagram.html)

### Applications and Tools

1. 👍 [mhutchie. vscode extension - Git Graph.](https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph)
1. 🌟 [Mermaid. Gitgraph Diagrams.](https://mermaid-js.github.io/mermaid/#/gitgraph)
1. 🌟 [visualizing-git.](https://git-school.github.io/visualizing-git/)
    1. [git-school/visualizing-git](https://github.com/git-school/visualizing-git)
1. [GitUp](https://gitup.co/)
    1. [git-up/GitUp](https://github.com/git-up/GitUp)
1. [nicoespeon/gitgraph.js](https://github.com/nicoespeon/gitgraph.js/)
1. [jubobs/gitdags](https://github.com/Jubobs/gitdags/wiki)
