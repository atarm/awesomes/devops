# Git

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=3 orderedList=true} -->

<!-- code_chunk_output -->

1. [是什么What](#是什么what)
2. [为什么Why](#为什么why)
3. [信条Beliefs/咒语Mantra/最佳实践Best Practices](#信条beliefs咒语mantra最佳实践best-practices)
4. [怎样学How](#怎样学how)
    1. [核心术语](#核心术语)
    2. [核心关注点](#核心关注点)
    3. [使用方法](#使用方法)
5. [Key Questions](#key-questions)
6. [Best Practices](#best-practices)
7. [Resources](#resources)
    1. [Collections](#collections)
    2. [Tips](#tips)
    3. [Cheatsheet](#cheatsheet)
    4. [Books and Tutorials](#books-and-tutorials)
    5. [Visualizations](#visualizations)
    6. [Playgrounds](#playgrounds)
    7. [Articles](#articles)

<!-- /code_chunk_output -->

## 是什么What

## 为什么Why

1. 免费且开源free and open source：
1. 极速且易扩展super fast and scalable：
1. 低成本的分支与合并cheap branching/merging：社会化编程（social programming）的必要条件之一

## 信条Beliefs/咒语Mantra/最佳实践Best Practices

1. branch early, and branch often
1. test && merge early, and test && merge often

## 怎样学How

### 核心术语

1. 5 areas：
    1. `stash`
    1. `working directory`
    1. `stage`
    1. `local repository`
    1. `remote repository`
1. `branch` and `HEAD`

### 核心关注点

1. how to tracking history
1. how to working together

### 使用方法

1. 命令行command line
1. 图形界面graphical user interface
1. 编辑器与IDE插件plugin of editor or IDE

## Key Questions

1. `stage`的价值是什么？（`stage`不记录`version`）
1. `git`相对于`svn`效率提升的功能有哪些？
1. 哪些功能是`git`提供的？哪些功能是`github`提供的？哪些功能是`git`没提供但是蕴含的？

## Best Practices

1. always use `--no-ff`

## Resources

### Collections

1. 👍 [git official document collection](https://git-scm.com/doc)
1. 👍 [GitHub Learning Lab](https://lab.github.com/): include many techniques resources besides `Git`, `GitHub`, `Markdown`, `Node` and so on

### Tips

1. 👍 [atlassian. Advanced Tips.](https://www.atlassian.com/git/tutorials/advanced-overview)
1. [Flight Rules for Git](https://github.com/k88hudson/git-flight-rules)

### Cheatsheet

1. [arslanbilal/git-cheat-sheet](https://github.com/arslanbilal/git-cheat-sheet)

### Books and Tutorials

#### Starter's Books and Tutorials

1. 👍 [阮一峰. 常用Git命令清单.](http://www.ruanyifeng.com/blog/2015/12/git-cheat-sheet.html)
1. 👍 [廖雪峰. Git教程.](https://www.liaoxuefeng.com/wiki/896043488029600)
1. 👍 [图解Git](http://marklodato.github.io/visual-git-guide/index-zh-cn.html)
1. [猴子都能懂的git入门](https://backlog.com/git-tutorial/cn/)
1. [git community book](http://shafiul.github.io/gitbook/index.html)
    1. [git community book中译本](http://gitbook.liuhui998.com/)
1. [Learn Git - Tutorials, Courses, and Books](https://gitconnected.com/learn/git)
1. [Atlassian-Bitbucket: Tutorials](https://www.atlassian.com/git/tutorials)
1. [Git Ready: learn git one commit at a time](http://gitready.com/)
1. [Learn Git Version Control using Interactive Browser-Based Scenarios](https://www.katacoda.com/courses/git)
1. [Git in The Trenches](http://cbx33.github.io/gitt/intro.html)

#### Advancer's Books and Tutorials

1. 👍 [阮一峰. Git原理入门.](https://www.ruanyifeng.com/blog/2018/10/git-internals.html)
1. 🌟 [Pro Git: An Open Source Book](https://git-scm.com/book/en/v2)

#### Expect's Books and Tutorials

### Visualizations

1. [Visualizing Git Concepts with D3](http://onlywei.github.io/explain-git-with-d3/)
    1. [Visualizing Git](http://git-school.github.io/visualizing-git/)

### Playgrounds

#### Starter's Playgrounds

1. [GitHub: Resources to learn Git](http://try.github.io/)
    1. 🌟 [Git-it: a (Mac, Win, Linux) Desktop App for Learning Git and GitHub](https://github.com/jlord/git-it-electron)
    1. 👍 🌟 [Learn Git branching](https://learngitbranching.js.org/) （可以通过`sandbox`保存进度）
1. [Getting started with git](https://play.instruqt.com/public/topics/getting-started-with-git)

#### Advancer's Playgrounds

1. 🌟 [Git exercises](https://gitexercises.fracz.com)

#### Expert's Playgrounds

### Articles

#### Workflow

1. [Introduction to GitLab Flow (with Git Flow& GitHub Flow Compared)](https://docs.gitlab.com/ee/topics/gitlab_flow.html)
1. [GitLab Workflow: An Overview](https://about.gitlab.com/blog/2016/10/25/gitlab-workflow-an-overview/)
1. [Understanding the GitHub flow](https://guides.github.com/introduction/flow/)
1. [5 types of Git workflow that will help you deliver better code](https://buddy.works/blog/5-types-of-git-workflows)

<!--
1. https://github.com/git/git-scm.com/issues/1239
-->
