# 版本控制系统Version Control System

## SCM and VCS

## 集中式版本控制系统Centralized Version Control System

1. `CVS`
1. `VSS(Visual SourceSafe)`
1. `SVN(SubVersioN)`

## 分布式版本控制系统Distributed Version Control System

1. `Git`
1. `Mercurial`/`HG`
1. `Bazaar`
1. `Darcs`

## CVCS vs DVCS
